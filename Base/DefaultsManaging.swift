//
//  DefaultsManager.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 26.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import Foundation
import Model

public protocol DefaultsManaging: AnyObject {
    var allUsers: [Drunkard] { get set }
    var name: String { get set }
    var drinks: [Drink] { get set }
    var weight: Double { get set }
    var gender: Gender { get set }
    var isFirstLaunch: Bool { get set }
    var hideAllExceptLast: Bool { get set }
    var selectedChartPeriod: TimePeriod { get set }
}

extension DefaultsManaging {
    func changeUser(name newName: String) {
        var users = allUsers
        
        if name != "" {
            let currentUser = Drunkard(name: name, drinks: drinks, weight: weight, gender: gender)
            
            if let index = users.firstIndex(where: { $0.name == name }) {
                users[index] = currentUser
            } else {
                users.append(currentUser)
            }
        }
        
        if newName.count > 0, let newUser = users.first(where: { $0.name == newName }) {
            name = newUser.name
            drinks = newUser.drinks
            weight = newUser.weight
            gender = newUser.gender
            
        } else {
            name = newName
            drinks = []
            weight = 70
            gender = .male
        }
        users.removeAll { $0.name == newName }
        allUsers = users
    }
}

public final class DefaultsManager: DefaultsManaging {
    public var allUsers: [Drunkard] {
        get {
            let users = decode(type: [Drunkard].self, forKey: "allUsers", processor: { data in
                try? (data as NSData).decompressed(using: .lzfse) as Data
            }) ?? []
            return users
        }
        set {
            encode(newValue, forKey: "allUsers", processor: { data in
                try? (data as NSData).compressed(using: .lzfse) as Data
            })
            NSUbiquitousKeyValueStore.default.synchronize()
        }
    }
    
    public var name: String {
        get {
            UserDefaults.standard.string(forKey: "name") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "name")
        }
    }
    
    public var drinks: [Drink] {
        get {
            if FeatureToggle.isOn(.loadFromFile) {
                if let path = Bundle.main.path(forResource: "data", ofType: "dat"),
                   let data = try? Data.init(contentsOf: URL(fileURLWithPath: path), options: []),
                   let loaded = try? JSONDecoder().decode([Drink].self, from: data) {
                    return loaded
                }
            }
            return decode(type: [Drink].self, forKey: "drinks", processor: { data in
                try? (data as NSData).decompressed(using: .lzfse) as Data
            }) ?? []
        }
        set {
            encode(newValue, forKey: "drinks", processor: { data in
                try? (data as NSData).compressed(using: .lzfse) as Data
            })
            NSUbiquitousKeyValueStore.default.synchronize()
        }
    }

    ///kg
    public var weight: Double {
        get {
            (NSUbiquitousKeyValueStore.default.object(forKey: "weight") as? NSNumber)?.doubleValue ?? 80
        }
        set {
            NSUbiquitousKeyValueStore.default.set(newValue, forKey: "weight")
            NSUbiquitousKeyValueStore.default.synchronize()
        }
    }
    
    public var gender: Gender {
        get {
            decode(type: Gender.self, forKey: "gender") ?? .male
        }
        set {
            encode(newValue, forKey: "gender")
            NSUbiquitousKeyValueStore.default.synchronize()
        }
    }
    
    public var isFirstLaunch: Bool {
        get {
            (UserDefaults.standard.value(forKey: "isFirstLaunch") as? Bool) ?? true
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isFirstLaunch")
        }
    }
    
    public var hideAllExceptLast: Bool {
        get {
            (UserDefaults.standard.value(forKey: "hideAllExceptLast") as? Bool) ?? true
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "hideAllExceptLast")
        }
    }
    
    public var selectedChartPeriod: TimePeriod {
        get {
            if let value = UserDefaults.standard.value(forKey: "selectedChartPeriod") as? Int,
               let period = TimePeriod(rawValue: value) {
                return period
            }
            return .week
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: "selectedChartPeriod")
        }
    }
    
    public init() { }
    
    private func encode<T: Codable>(_ object: T, forKey: String, processor: ((Data) -> Data?)? = nil) {
        if var data = try? JSONEncoder().encode(object) {
            if let processed = processor?(data) {
                data = processed
            }
            NSUbiquitousKeyValueStore.default.set(data, forKey: forKey)
        }
    }
    
    private func decode<T: Codable>(type: T.Type, forKey: String, processor: ((Data) -> Data?)? = nil) -> T? {
        if var data = NSUbiquitousKeyValueStore.default.object(forKey: forKey) as? Data {
            if let processed = processor?(data) {
                data = processed
            }
            if let loaded = try? JSONDecoder().decode(T.self, from: data) {
                return loaded
            }
        }
        return nil
    }
}

public final class DefaultsManagerDemo: DefaultsManaging {
    public var allUsers: [Drunkard] = []
    public var name: String = "Demo"
    public var drinks: [Drink] = []
    public var weight: Double = 70
    public var gender: Gender = .male
    public var isFirstLaunch: Bool = false
    public var hideAllExceptLast: Bool = false
    public var selectedChartPeriod: TimePeriod = .week
    
    public init() { }
}
