//
//  Preprocessing.swift
//  Alcohelper
//
//  Created by kogami on 13.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

// MARK: - FeatureToggle

public struct FeatureToggle {
    public enum Feature {
        case loadFromFile
        case demoModeOnSimulator
    }
    
    public static func isOn(_ feature: Feature) -> Bool {
        switch feature {
        case .loadFromFile:
            return false
        case .demoModeOnSimulator:
            return false
        }
    }
}

// MARK: - Environment

public enum Environment {
    public static var isSimulator: Bool {
        #if targetEnvironment(simulator)
        return true
        #else
        return false
        #endif
    }
    public static var isDebug: Bool {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }
}
