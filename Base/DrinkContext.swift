//
//  DrinkCoordinator.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 31.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Model

public protocol DrinkContextDelegate {
    func userChanged()
    func startDemo()
}

public final class DrinkContext {
    public let defaultsManager: DefaultsManaging
    public var drinkManager: DrinkManager
    
    public var delegate: DrinkContextDelegate?
    
    public init(defaultsManager: DefaultsManaging, drinkSource: (() -> [Drink])? = nil) {
        self.defaultsManager = defaultsManager
        self.drinkManager = DrinkManager(with: drinkSource ?? { defaultsManager.drinks })
        self.drinkManager.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(Self.updateKVStoreItems(_:)), name: NSUbiquitousKeyValueStore.didChangeExternallyNotification, object: nil)
    }
    
    @objc
    private func updateKVStoreItems(_ items: NSNotification) {
        drinkManager.setup(with: defaultsManager.drinks)
    }
    
    public func changeUser(with name: String) {
        defaultsManager.changeUser(name: name)
        delegate?.userChanged()
    }
    
    public func startDemo() {
        delegate?.startDemo()
    }
}

extension DrinkContext: DrinkManagerDelegate {
    public func drinkManager(drinksChanged drinks: [Drink]) {
        self.defaultsManager.drinks = drinks
    }
}
