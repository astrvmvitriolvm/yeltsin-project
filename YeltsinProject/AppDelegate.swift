//
//  AppDelegate.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 23.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit
import CoreData
import Costelique
import Base
import Model
import UI

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    private var context: DrinkContext! {
        didSet {
            context.delegate = self
        }
    }
    private var router: AppRouter!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UserDefaults.standard.set(["ru"], forKey: "AppleLanguages")
        UserDefaults.standard.set("ru_RU", forKey: "AppleLocale")
        if Environment.isSimulator, FeatureToggle.isOn(.demoModeOnSimulator) {
            enterDemoMode()
        } else {
            startRouting()
        }
        return true
    }
    
    private func startRouting(splashScreenDelay: Double = 1) {
        context = DrinkContext(defaultsManager: DefaultsManager())
        router = AppRouter(context: context)
        router.start(with: &window, splashScreenDelay: splashScreenDelay)
    }
    
    private func enterDemoMode() {
        context = DrinkContext(defaultsManager: DefaultsManagerDemo(), drinkSource: generateRandomDrinks)
        router = AppRouter(context: context)
        router.start(with: &window, splashScreenDelay: 3)
    }
}

extension AppDelegate: DrinkContextDelegate {
    func userChanged() {
        startRouting(splashScreenDelay: 0.3)
    }
    
    func startDemo() {
        enterDemoMode()
    }
}

private extension AppDelegate {
    func generateRandomDrinks() -> [Drink] {
        AKTimer.start()
        var randgen = Xorshift128Plus()
        let random64: () -> UInt64 = {
            randgen.next()
        }
        let random32: () -> UInt32 = {
            UInt32(randgen.next() % UInt64(UInt32.max))
        }
        let percents: [Float] = [3, 4.5, 6, 8.4, 9, 12, 16, 20, 40, 45, 70]
        var drinks: [Drink] = []
        let count = 100_000 - 2
            
        while drinks.count < count {
            let randomInterval = Double(random64() % UInt64(count * 13500))
            let date = Date().addingTimeInterval(-.day * 4 - randomInterval)
            if sin(date.timeIntervalSinceReferenceDate * 2 * .pi / .week) + 1 < (Double(random64() % 1001) / 1000) * 1.5 + 0.5 {
                continue
            }
            let index = Int(random32()) % percents.count
            let percent = percents[index]
            let ml = 50 + Float(random64() % 3000) / Float(percent) / 50 * 50
            let zakus = Float((random64() % 101) / 50)
            let endDate: Date? = date.addingTimeInterval(Double(ml * 10))
            drinks.append(Drink(milliliters: ml, percents: percent, date: date, zakus: zakus, endDate: endDate))
        }
        drinks.append(Drink(milliliters: 500, percents: 5, date: Date().addingTimeInterval(-.minute * 120), zakus: 0, endDate: Date().addingTimeInterval(-.minute * 60)))
        drinks.append(Drink(milliliters: 500, percents: 5, date: Date().addingTimeInterval(-.minute * 60), zakus: 0, endDate: Date().addingTimeInterval(-.minute * 50)))
        AKTimer.log("generateRandomDrinks() \(drinks.count)")
        return drinks
    }
}
