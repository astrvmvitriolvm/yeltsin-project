//
//  Identifiable.swift
//  Base
//
//  Created by Данис Тазетдинов on 08.03.2018.
//  Copyright © 2018 Danis Tazetdinov. All rights reserved.
//

import UIKit

public protocol Identifiable {
    static var classIdentifier: String { get }
}

extension Identifiable {
    public static var classIdentifier: String {
        return String(describing: self)
    }
}

extension UIViewController: Identifiable { }

extension UIView: Identifiable { }
