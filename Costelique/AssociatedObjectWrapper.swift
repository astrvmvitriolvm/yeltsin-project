//
//  AssociatedObjectWrapper.swift
//  Base
//
//  Created by Матвеев Олег on 06.08.2021.
//  Copyright © 2021 Danis Tazetdinov. All rights reserved.
//

import UIKit

@propertyWrapper
public struct AssociatedObject<T: NSObject> {
    var host = NSObject()
    let policy: objc_AssociationPolicy
    
    public var wrappedValue: T? {
        mutating set {
            objc_setAssociatedObject(host, &host, newValue, policy)
        }
        mutating get {
            objc_getAssociatedObject(host, &host) as? T
        }
    }

    public init(wrappedValue: T?) {
        self.init(wrappedValue: wrappedValue, policy: .OBJC_ASSOCIATION_RETAIN)
    }
    
    public init(wrappedValue: T?, policy: objc_AssociationPolicy) {
        objc_setAssociatedObject(host, &host, wrappedValue, policy)
        self.policy = policy
    }
}
