//
//  TextStyleFactory.swift
//  UI
//
//  Created by Матвеев Олег on 16.02.2021.
//  Copyright © 2021 Danis Tazetdinov. All rights reserved.
//

import UIKit

public struct TextStyler {
    public enum Attribute {
        case font(size: CGFloat, weight: UIFont.Weight = .regular)
        case customFont(UIFont)
        case color(UIColor)
        case lineHeight(CGFloat)
        case kerning(CGFloat)
        case alignment(NSTextAlignment)
        case lineBreakMode(NSLineBreakMode)
    }
    
    private let textAttributes: [Attribute]
    private var rangedAttributes: [(NSRange, [Attribute])]?
    
    private init(_ attributes: [Attribute]) {
        textAttributes = attributes
    }
    
    public init(_ attributes: Attribute...) {
        textAttributes = attributes
    }
    
    public func appending(_ attributes: Attribute...) -> TextStyler {
        var newAttributes = textAttributes
        newAttributes.append(contentsOf: attributes)
        return TextStyler(newAttributes)
    }
    
    public mutating func append(_ attributes: Attribute...) {
        var newAttributes = textAttributes
        newAttributes.append(contentsOf: attributes)
        self = TextStyler(newAttributes)
    }
    
    public mutating func append(_ attributes: Attribute..., inRange: NSRange) {
        if rangedAttributes == nil {
            rangedAttributes = []
        }
        rangedAttributes!.append((inRange, attributes))
    }

    //Generating attributes
    private func attributes(from: [Attribute]) -> [NSAttributedString.Key: Any] {
        var attributes: [NSAttributedString.Key: Any] = [:]
        var paragraphStyle: NSMutableParagraphStyle?
        
        var paragraph: NSMutableParagraphStyle {
            if let style = paragraphStyle {
                return style
            }
            let style = NSMutableParagraphStyle()
            paragraphStyle = style
            return style
        }
        
        for attribute in self.textAttributes {
            switch attribute {
            case .font(let size, let weight):
                attributes[.font] = UIFont.systemFont(ofSize: size, weight: weight)
                
            case .customFont(let value):
                attributes[.font] = value
                
            case .color(let value):
                attributes[.foregroundColor] = value
                
            case .kerning(let value):
                attributes[.kern] = value
                
            case .lineHeight(let value):
                paragraph.minimumLineHeight = value
                paragraph.maximumLineHeight = value
                
            case .alignment(let value):
                paragraph.alignment = value
                
            case .lineBreakMode(let value):
                paragraph.lineBreakMode = value
            }
        }
        
        if let style = paragraphStyle {
            attributes[.paragraphStyle] = style
        }
        
        return attributes
    }
    
    //Only attributes
    public func attributes() -> [NSAttributedString.Key: Any] {
        attributes(from: textAttributes)
    }
    
    //For string with ranged attributes
    public func attributed(_ string: String) -> NSAttributedString {
        if let ranged = rangedAttributes {
            let string = NSMutableAttributedString(string: string, attributes: attributes())
            for (range, attributes) in ranged {
                string.addAttributes(self.attributes(from: attributes), range: range)
            }
            return string
        } else {
            return NSAttributedString(string: string, attributes: attributes())
        }
    }
    
    public func mutable(_ string: String) -> NSMutableAttributedString {
        let string = NSMutableAttributedString(string: string, attributes: attributes())
        if let ranged = rangedAttributes {
            for (range, attributes) in ranged {
                string.addAttributes(self.attributes(from: attributes), range: range)
            }
        }
        return string
    }
}
