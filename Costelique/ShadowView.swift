//
//  ShadowView.swift
//  UI
//
//  Created by Матвеев Олег on 12.02.2021.
//  Copyright © 2021 Danis Tazetdinov. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    private enum Constants {
        static let shadowRadius: CGFloat = 10
        static let shadowOpacity: CGFloat = 0.06
        static let borderShadowRadius: CGFloat = 1
        static let borderShadowOpacity: CGFloat = 0.2
    }
    
    private var backgroundLayer = CAShapeLayer()
    private var shadows: [CALayer] = []
    private var radius: CGFloat = 0
    private var corners: UIRectCorner = .allCorners
        
    override var frame: CGRect {
        didSet {
            if oldValue != frame {
                if let animation = self.layer.animation(forKey: "bounds.size") as? CABasicAnimation {
                    animate(animation)
                } else {
                    updatePath()
                }
            }
        }
    }
    override var bounds: CGRect {
        didSet {
            if oldValue != bounds {
                if let animation = self.layer.animation(forKey: "bounds.size") as? CABasicAnimation {
                    animate(animation)
                } else {
                    updatePath()
                }
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        layer.insertSublayer(backgroundLayer, at: .zero)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        translatesAutoresizingMaskIntoConstraints = false
        layer.insertSublayer(backgroundLayer, at: .zero)
    }
    
    func setup(background: UIColor, cornerRadius: CGFloat, corners: UIRectCorner = .allCorners) {
        clipsToBounds = false
        backgroundLayer.fillColor = background.cgColor
        self.radius = cornerRadius
        self.corners = corners
        updatePath()
    }
    
    func removeShadow() {
        shadows.forEach { $0.removeFromSuperlayer() }
        shadows = []
    }
    
    func applyShadowWithBorder() {
        removeShadow()
        appendShadow(shadowRadius: Constants.shadowRadius, shadowOpacity: Constants.shadowOpacity)
        appendShadow(shadowRadius: Constants.borderShadowRadius, shadowOpacity: Constants.borderShadowOpacity)
    }
    
    func appendShadow(shadowRadius: CGFloat,
                      shadowOpacity: CGFloat,
                      shadowColor: UIColor = .black,
                      shadowOffset: CGSize = .zero) {
        let layer = CALayer()
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = Float(shadowOpacity)
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        shadows.append(layer)
        self.layer.insertSublayer(layer, below: backgroundLayer)
        updatePath()
    }
    
    private func updatePath() {
        let path: CGPath
        if radius == 0 {
            path = UIBezierPath(rect: bounds).cgPath
        } else {
            let radii = CGSize(width: radius, height: radius)
            path = UIBezierPath(roundedRect: bounds, byRoundingCorners: self.corners, cornerRadii: radii).cgPath
        }
        for layer in shadows {
            layer.shadowPath = path
        }
        backgroundLayer.path = path
    }
    
    private func animate(_ animation: CABasicAnimation) {
        //TODO: Animations
        updatePath()
    }
}

extension UIView {
    private enum Constants {
        static let shadowViewTag = 0x73686477 // "shdw" in hex
    }
    
    func addShadowWithBorder() {
        if viewWithTag(Constants.shadowViewTag) != nil {
            return
        }
        
        let shadowView = ShadowView()
        shadowView.applyShadowWithBorder()
        shadowView.setup(background: backgroundColor ?? .clear, cornerRadius: layer.cornerRadius)
        shadowView.tag = Constants.shadowViewTag
        
        addSubview(shadowView)
        sendSubviewToBack(shadowView)
        shadowView.constraintsToSuperview(withInsets: .zero).activate()
    }
    
    func removeShadowWithBorder() {
        if let view = viewWithTag(Constants.shadowViewTag) {
            view.removeFromSuperview()
        }
    }
}
