//
//  AKTimer.swift
//  Costelique
//
//  Created by kogami on 14.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public extension AKTimer {
    private static var startDate: CFAbsoluteTime = .zero
    
    static func start() {
        startDate = CFAbsoluteTimeGetCurrent()
    }
    
    static func log(_ log: String? = nil, fileName: String = #file, functionName: String = #function) {
        let interval = CFAbsoluteTimeGetCurrent() - startDate
    
        var string = String(format: "AKTimer: %f ", interval)
        if let log = log {
            string += log
        } else {
            let filename = fileName.components(separatedBy: "/").last ?? ""
            string += filename + " " + functionName
        }
        print(string)
        startDate = CFAbsoluteTimeGetCurrent()
    }
}

public class AKTimer {
    public init() {
        date = CFAbsoluteTimeGetCurrent()
    }
    
    private var date: CFAbsoluteTime = .zero
    private var keys: [String: Double] = [:]
    private var keysOrdered: [String] = []
    private var times: Int = 0
    
    public func start() {
        date = CFAbsoluteTimeGetCurrent()
        times += 1
    }
    
    public func saveTime(_ key: String) {
        let interval = CFAbsoluteTimeGetCurrent() - date
        keys[key] = (keys[key] ?? 0) + interval
        if !keysOrdered.contains(key) {
            keysOrdered.append(key)
        }
        date = CFAbsoluteTimeGetCurrent()
    }
    
    public func log(_ key: String? = nil, average: Bool = true) {
        if times == 0 {
            let interval = CFAbsoluteTimeGetCurrent() - date
            print("\(String(format: "AKTimer: %f %@", interval, key ?? ""))")
            return
        }
        let allTime = keys.reduce(0, { $0 + $1.value })
        for key in keysOrdered {
            let time = keys[key]! / (average ? Double(times) : 1)
            print("\(String(format: "AKTimer:      %f", time)) - \(key)")
        }
        let time = allTime / (average ? Double(times) : 1)
        print(String(format: "AKTimer: %f, repeats: %d - ", time, times) + (key ?? ""))
    }
}
