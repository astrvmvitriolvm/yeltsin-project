//
//  Property.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 30.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public class Observable<T> {
    private struct BindedListener {
        weak var target: AnyObject?
        let listener: (T) -> Void
    }
    
    private class Storage {
        var listeners: [BindedListener] = []
    }
    
    private let syncQueue = DispatchQueue(label: "ak.costelique.observable.sync", qos: .userInteractive, attributes: [.concurrent])
    
    private let storage = Storage()
    private var value: T
    
    public var wrappedValue: T {
        get {
            syncQueue.sync { value }
        }
        set {
            var listeners: [(T) -> Void] = []
            syncQueue.sync(flags: .barrier) {
                value = newValue
                storage.listeners = storage.listeners.filter {
                    guard $0.target != nil else {
                        return false
                    }
                    listeners.append($0.listener)
                    return true
                }
            }
            listeners.forEach({ $0(newValue) })
        }
    }
    
    public init(_ wrappedValue: T) {
        self.value = wrappedValue
    }
    
    public func addObserver<N: AnyObject>(_ target: N?, skipCurrent: Bool = false, listener: @escaping (N, T) -> Void) {
        guard let target = target else {
            return
        }
        syncQueue.sync(flags: .barrier) {
            storage.listeners.append(BindedListener(target: target, listener: { [weak target] value in
                guard let target = target else {
                    return
                }
                listener(target, value)
            }))
        }
        
        if !skipCurrent {
            listener(target, wrappedValue)
        }
    }
    
    public func remove(listener: AnyObject) {
        syncQueue.sync(flags: .barrier)  {
            storage.listeners = storage.listeners.filter({ $0.target !== listener && $0.target != nil })
        }
    }
}
