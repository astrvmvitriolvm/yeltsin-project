//
//  WeakRef.swift
//  MegaGame
//
//  Created by Oleg Matveev on 09/04/2019.
//  Copyright © 2019 Megastar. All rights reserved.
//

import Foundation

//for weak arrays

public protocol Weakly {
    associatedtype GenericType: AnyObject
    var value: GenericType? { get }
}

public struct WeakRef<T>: Weakly where T: AnyObject {
    public typealias GenericType = T
    public private(set) weak var value: T?

    init(value: T?) {
        self.value = value
    }
}

public extension Array where Element: Weakly {
    mutating func clean() {
        self = filter { $0.value != nil }
    }
    
    mutating func reap() -> [Element.GenericType] {
        var objects: [Element.GenericType] = []

        self = filter {
            guard let value = $0.value else {
                return false
            }
            objects.append(value)
            return true
        }
        
        return objects
    }
}

public extension Array where Element: Weakly, Element.GenericType: Equatable {
    func contains(_ value: Element.GenericType) -> Bool {
        contains(where: { $0.value == value })
    }
}

public extension Array where Element: AnyObject {
    func weakify() -> [WeakRef<Element>] {
        map { WeakRef(value: $0) }
    }
}
