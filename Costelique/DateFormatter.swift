//
//  Formatters.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 27.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit

public extension DateFormatter {
    private enum Format: String {
        case time = "HH:mm"
        case timeAndDate = "HH:mm dd MMM"
        case dateAndTime = "dd MMM HH:mm"
        case dayMounthLong = "dd MMMM"
        case dayMounthShort = "dd MM"
        case year = "YYYY"
    }
    
    private static func formatter(with template: Format) -> DateFormatter {
        let locale = NSLocale(localeIdentifier: "RU_ru") as Locale
        let format = DateFormatter.dateFormat(fromTemplate: template.rawValue, options: 0, locale: locale)
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter
    }
    
    static let timeFormatter: DateFormatter = formatter(with: .time)
    static let dateAndTimeFormatter: DateFormatter = formatter(with: .dateAndTime)
    static let timeAndDateFormatter: DateFormatter = formatter(with: .timeAndDate)
    static let dayMonthLong: DateFormatter = formatter(with: .dayMounthLong)
    static let dayMounthShort: DateFormatter = formatter(with: .dayMounthShort)
    static let year: DateFormatter = formatter(with: .year)
    
    static func timeAndDateIfToday(_ from: Date) -> String {
        if from.isSameDay(Date()) {
            return timeFormatter.string(from: from)
        } else {
            return timeAndDateFormatter.string(from: from)
        }
    }
}

