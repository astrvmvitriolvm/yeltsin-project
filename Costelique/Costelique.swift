//
//  Costelique.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 25.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit

public extension Collection {
    subscript (safe index: Index) -> Element? {
        indices.contains(index) ? self[index] : nil
    }
}

public extension UIView {
    func firstResponder() -> UIView? {
        isFirstResponder ? self : subviews.compactMap({ $0.firstResponder() }).first
    }
    
    func removeAllSubviews() {
        subviews.reversed().forEach { $0.removeFromSuperview() }
    }
    
    func constraintsToSuperview(withInsets insets: UIEdgeInsets) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            return []
        }
        return [
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: insets.left),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -insets.right),
            topAnchor.constraint(equalTo: superview.topAnchor, constant: insets.top),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -insets.bottom)
        ]
    }
}

public extension Date {
    func isFirst(in component: Calendar.Component) -> Bool {
        switch component {
        case .day:
            return isStartOfWeek()

        case .weekOfYear:
            return isStartOfMonth()

        case .month:
            return isStartOfYear()

        default:
            return false
        }
    }

    func isSameDay(_ date: Date, calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: .day)
    }

    func isSameWeek(_ date: Date, calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }

    func isSameMonth(_ date: Date, calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: .month)
    }

    func isSameYear(_ date: Date, calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: .year)
    }
    
    func round(to: Calendar.Component, calendar: Calendar = .current) -> Date {
        calendar.dateInterval(of: to, for: self)!.start
    }

    func isStartOfWeek(using calendar: Calendar = .current) -> Bool {
        let startOfWeek = calendar.dateComponents([.calendar, .yearForWeekOfYear, .weekOfYear], from: self).date!
        return isSameDay(startOfWeek)
    }

    func isStartOfMonth(using calendar: Calendar = .current) -> Bool {
        let startOfMonth = calendar.dateComponents([.calendar, .month], from: self).date!
        return isSameDay(startOfMonth)
    }

    func isStartOfYear(using calendar: Calendar = .current) -> Bool {
        let startOfYear = calendar.dateComponents([.calendar, .year], from: self).date!
        return isSameDay(startOfYear)
    }
}

public extension TimeInterval {
    static let minute: TimeInterval = 60
    static let hour: TimeInterval = minute * 60
    static let day: TimeInterval = hour * 24
    static let week: TimeInterval = day * 7
}

public extension UIApplication {
    var window: UIWindow? {
        windows.first(where: { $0.isKeyWindow })
    }
}

public extension UIViewController {
    static func instantinateFromNib() -> Self {
        self.init(nibName: classIdentifier, bundle: Bundle(for: self))
    }
}

public extension Float {
    func rounded(digits: Int) -> Float {
        let divider = pow(10, Float(digits))
        return (self * divider).rounded() / divider
    }
}

public extension Double {
    func rounded(digits: Int) -> Double {
        let divider = pow(10, Double(digits))
        return (self * divider).rounded() / divider
    }
}

public extension String {
    func replacingAll(where condition: (Character) -> Bool) -> String {
        var string = self
        string.removeAll(where: condition)
        return string
    }
}

