//
//  Atomic.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 30.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

@propertyWrapper
public class Atomic<Value> {
    public var projectedValue: Atomic<Value> {
        return self
    }
    private let queue = DispatchQueue(label: "ru.costelique.atomic")
    private var value: Value

    public init(wrappedValue: Value) {
        self.value = wrappedValue
    }
    
    public var wrappedValue: Value {
        get {
            return queue.sync { value }
        }
        set {
            queue.sync { value = newValue }
        }
    }
    
    public func mutate(_ mutation: (inout Value) -> Void) {
        return queue.sync {
            mutation(&value)
        }
    }
}
