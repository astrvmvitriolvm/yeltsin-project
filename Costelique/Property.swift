//
//  Property.swift
//  Costelique
//
//  Created by Матвеев Олег on 22.06.2022.
//  Copyright © 2022 Oleg Matveev. All rights reserved.
//

import Foundation

public class Property<Value: Any> {
    private struct TargetedListener {
        weak var target: AnyObject?
        let listener: (Value) -> Void
    }
    
    private let syncQueue = DispatchQueue(label: "ru.costelique.property.sync", qos: .userInteractive)
    private var listeners: [TargetedListener] = []
    private var _value: Value
    
    public fileprivate(set) var value: Value {
        get {
            syncQueue.sync { _value }
        }
        set {
            syncQueue.sync { () -> [(Value) -> Void] in
                _value = newValue
                var listeners: [(Value) -> Void] = []
                self.listeners = self.listeners.filter {
                    guard $0.target != nil else {
                        return false
                    }
                    listeners.append($0.listener)
                    return true
                }
                return listeners
            }.forEach { $0(newValue) }
        }
    }
    
    fileprivate init(_ emitter: AbstractEmitter<Value>) {
        _value = emitter.value
        emitter.emitterBlock = { [weak self] value in
            guard let strongSelf = self else {
                return
            }
            strongSelf.value = value
        }
    }
    
    fileprivate init(_ value: Value) {
        _value = value
    }
    
    public func addListener<N: AnyObject>(_ target: N?, skipCurrent: Bool = false, _ listener: @escaping (N, Value) -> Void) {
        guard let target = target else {
            return
        }
        syncQueue.sync {
            listeners.append(TargetedListener(target: target) { [weak target] value in
                guard let target = target else {
                    return
                }
                listener(target, value)
            })
        }
        
        if !skipCurrent {
            listener(target, value)
        }
    }

    public func remove(listener: AnyObject) {
        syncQueue.sync {
            listeners = listeners.filter { $0.target !== listener && $0.target != nil }
        }
    }
    
    public required init<Old: Property<OldValue>, OldValue>(property: Old, transform: @escaping (OldValue) -> Value, validation: ((OldValue) -> Bool)? = nil) {
        _value = transform(property.value)
        property.addListener(self, skipCurrent: true) { strongSelf, value in
            if validation?(value) ?? true {
                strongSelf.value = transform(value)
            }
        }
    }
}

public class AbstractEmitter<T> {
    fileprivate var emitterBlock: ((T) -> Void)?
    
    public private(set) lazy var property: Property<T> = {
        Property(self)
    }()
    
    private let valueQueue = DispatchQueue(label: "ru.goods.abstractemitter.value", qos: .userInteractive, attributes: [.concurrent])
    private var _value: T
    
    public var value: T {
        get {
            valueQueue.sync { _value }
        }
        set {
            valueQueue.sync(flags: .barrier) { _value = newValue }
        }
    }

    public required init(_ value: T) {
        _value = value
        assert(type(of: self) != AbstractEmitter.self)
    }
}

public final class Emitter<E: Equatable>: AbstractEmitter<E> {
    override public var value: E {
        didSet {
            if value != oldValue {
                emitterBlock?(value)
            }
        }
    }
}

public final class EveryEmitter<A>: AbstractEmitter<A> {
    override public var value: A {
        didSet {
            emitterBlock?(value)
        }
    }
}
