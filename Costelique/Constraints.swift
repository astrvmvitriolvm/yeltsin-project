//
//  Constraints.swift
//  Costelique
//
//  Created by kogami on 14.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit

public extension NSLayoutConstraint {
    class func activate(_ constraints: [NSLayoutConstraint?]) {
        NSLayoutConstraint.activate(constraints.compactMap { $0 })
    }

    class func deactivate(_ constraints: [NSLayoutConstraint?]) {
        NSLayoutConstraint.deactivate(constraints.compactMap { $0 })
    }
}

public extension NSLayoutConstraint {
    func activate() {
        isActive = true
    }
    func deactivate() {
        isActive = false
    }
}

public extension Array where Element == NSLayoutConstraint {
    func activate() {
        forEach { $0.activate() }
    }
    func deactivate() {
        forEach { $0.deactivate() }
    }
}

public extension Array where Element == NSLayoutConstraint? {
    func activate() {
        forEach { $0?.activate() }
    }
    func deactivate() {
        forEach { $0?.deactivate() }
    }
}
