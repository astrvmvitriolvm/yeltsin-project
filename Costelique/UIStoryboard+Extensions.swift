//
//  UIStoryboard+Extensions.swift
//  Base
//
//  Created by Данис Тазетдинов on 10.03.2018.
//  Copyright © 2018 Danis Tazetdinov. All rights reserved.
//

import UIKit

public extension UIStoryboard {
    static let main = UIStoryboard(name: "Main", bundle: nil)

    func instantiate<T: UIViewController>(_ type: T.Type) -> T {
        let id = T.classIdentifier
        guard let vc = instantiateViewController(withIdentifier: id) as? T else {
            fatalError("could not construct \(T.self)")
        }
        return vc
    }
}
