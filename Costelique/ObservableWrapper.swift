//
//  ObservableWrapper.swift
//  Costelique
//
//  Created by Матвеев Олег on 04.04.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation


@propertyWrapper struct Capitalized {
    var wrappedValue: String {
        didSet { wrappedValue = wrappedValue.capitalized }
    }

    init(wrappedValue: String) {
        self.wrappedValue = wrappedValue.capitalized
    }
}
