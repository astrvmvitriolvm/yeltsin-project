//
//  SelectorViewController.swift
//  UI
//
//  Created by Данис Тазетдинов on 03.09.2018.
//  Copyright © 2018 Danis Tazetdinov. All rights reserved.
//

import UIKit

public final class SelectorViewController: UIViewController {
    public enum Mode {
        case fullScreen
        case requiredTopMargin
        case requiredTopMarginWithScrollView(UIScrollView)
    }
    
    public enum BackgroundMode {
        case clear
        case darkening
        case blured
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 20
        static let horizontalSpacing: CGFloat = 15.0
        static let verticalSpacing: CGFloat = 16.0
        static let verticalPanAllowance: CGFloat = 80.0
        static let titleSpacing: CGFloat = 8.0
        static let titleTopOffset: CGFloat = 7.0
        
        static let handleWidth: CGFloat = 40.0
        static let handleHeight: CGFloat = 4.0
        static let handleTopOffset: CGFloat = 7.0
        
        static let bgViewRequiredTopMargin: CGFloat = 54.0
        
        static let draggableHeight: CGFloat = 150.0
        static let dismissHeight: CGFloat = 24.0
        static let buttonImageTopInset: CGFloat = 2
        static let buttonImageLeftInset: CGFloat = -16.0
        
        static let resetAnimationDuration = 0.2
        
        static let titleImageSideLength: CGFloat = 34.0
        
        static let nonEmptyTopVerticalOffset: CGFloat = 16.0
        static let emptyTopVerticalOffset: CGFloat = 0.0
    }
    
    fileprivate let contentVC: UIViewController
    private var contentVCTopToTitleBottomConstraint: NSLayoutConstraint?
    private var contentVCHeightConstraint: NSLayoutConstraint?
    private var titleImageTopConstraint: NSLayoutConstraint?
    
    // outlets
    fileprivate weak var blurredBackground: UIVisualEffectView!
    fileprivate weak var contentContainerView: UIView!
    private weak var contentBackgroundView: UIVisualEffectView!
    private weak var draggableView: UIView!
    private weak var handleView: UIView!
    private weak var titleLabel: UILabel!
    private weak var closeButton: UIButton!
    
    private var titleImage: UIImage?
    private var shouldHideCloseButton = true
    private var mode: Mode = .requiredTopMargin
    private var backgroundMode: BackgroundMode = .clear
    private var panelBackground: BackgroundMode = .blured
    private var bottomDismissScrollViewPanGestureRecognizer: UIPanGestureRecognizer?
    private var draggablePanGestureRecognizer: UIPanGestureRecognizer?
    private var animator: SelectorTransition?
    
    public var defaultCompletion: (() -> Void)?
    
    // MARK: - Lifecycle
    
    private init(viewController: UIViewController) {
        contentVC = viewController
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("SelectorViewController does not support NSCoding")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        let gr = UITapGestureRecognizer(target: self, action: #selector(Self.tapOnBackground(_:)))
        gr.cancelsTouchesInView = false
        view.addGestureRecognizer(gr)
        setupView()
    }
    
    private func updateTopConstraints() {
        if contentVC.title?.isEmpty ?? true && closeButton.isHidden {
            contentVCTopToTitleBottomConstraint?.constant = Constants.emptyTopVerticalOffset
        } else {
            contentVCTopToTitleBottomConstraint?.constant = Constants.nonEmptyTopVerticalOffset
        }
    }
    
    private func setupView() {
        let bgView = UIVisualEffectView()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.clipsToBounds = true
        bgView.backgroundColor = .clear
        bgView.effect = nil
        blurredBackground = bgView
        view.addSubview(blurredBackground)
        
        let contentView = UIView(frame: .zero)
        contentView.clipsToBounds = true
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = .clear// UIColor.systemBackground
        contentView.layer.cornerRadius = Constants.cornerRadius
        contentContainerView = contentView
        view.addSubview(contentContainerView)
        
        let contentBackgroundView = UIVisualEffectView()
        contentBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        self.contentBackgroundView = contentBackgroundView
        contentView.addSubview(contentBackgroundView)
        contentBackgroundView.constraintsToSuperview(withInsets: .zero).activate()
        
        let bottomConstant = Constants.cornerRadius + Constants.verticalPanAllowance
        NSLayoutConstraint.activate([
            blurredBackground.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            blurredBackground.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            blurredBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomConstant),
            blurredBackground.topAnchor.constraint(equalTo: view.topAnchor),
            
            contentContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contentContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomConstant),
        ])
        
        setupContentViewSubviews(in: contentContainerView)
        updateTopConstraints()

        draggableView?.isHidden = mode.isFullScreen
        
        switch mode {
        case .fullScreen:
            contentContainerView.topAnchor.constraint(equalTo: view.topAnchor).activate()
            
        case .requiredTopMargin, .requiredTopMarginWithScrollView:
            contentContainerView.topAnchor.constraint(greaterThanOrEqualTo: view.topAnchor, constant: Constants.bgViewRequiredTopMargin).activate()
        }
        
        switch panelBackground {
        case .blured:
            contentBackgroundView.effect = UIBlurEffect(style: .systemChromeMaterial)
            
        case .darkening:
            contentBackgroundView.backgroundColor = .systemBackground
            
        case .clear:
            contentBackgroundView.backgroundColor = .clear
        }
    }
    
    private func setupContentViewSubviews(in containerView: UIView) {
        let draggable = UIView(frame: .zero)
        draggable.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(draggable)
        draggableView = draggable
        let draggablePanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(Self.pan(_:)))
        containerView.addGestureRecognizer(draggablePanGestureRecognizer)
        draggablePanGestureRecognizer.delegate = self
        self.draggablePanGestureRecognizer = draggablePanGestureRecognizer
        
        contentVC.willMove(toParent: self)
        contentVC.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(contentVC)
        contentVC.view.setContentHuggingPriority(.defaultLow, for: .vertical)
        containerView.addSubview(contentVC.view)
        contentVC.didMove(toParent: self)
        contentVC.view.backgroundColor = .clear
        
        let titleImageView = UIImageView(frame: .zero)
        titleImageView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(titleImageView)
        var titleSpacing: CGFloat = 0.0
        var titleImageWidth: CGFloat = 0.0
        if titleImage != nil {
            titleImageView.image = titleImage
            titleSpacing = 3.0
            titleImageWidth = Constants.titleImageSideLength
        }
        titleImageTopConstraint = titleImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: Constants.verticalSpacing)
        
        let title = UILabel(frame: .zero)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont.systemFont(ofSize: 20.0, weight: .medium)
        title.setContentHuggingPriority(.defaultHigh + 1, for: .vertical)
        title.setContentHuggingPriority(.defaultLow, for: .horizontal)
        title.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        title.setContentCompressionResistancePriority(.required, for: .vertical)
        title.text = contentVC.title
        title.numberOfLines = 2
        containerView.addSubview(title)
        titleLabel = title
        
        contentVCHeightConstraint = contentVC.view.heightAnchor.constraint(equalToConstant: contentVC.preferredContentSize.height)
        contentVCHeightConstraint?.priority = .defaultHigh - 1
        contentVCTopToTitleBottomConstraint = contentVC.view.topAnchor.constraint(equalTo: title.bottomAnchor)
        
        let handle = UIView(frame: .zero)
        handle.translatesAutoresizingMaskIntoConstraints = false
        handle.backgroundColor = UIColor.systemGray
        handle.layer.cornerRadius = Constants.handleHeight / 2
        handle.isHidden = !shouldHideCloseButton
        draggable.addSubview(handle)
        handleView = handle
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.imageEdgeInsets.left += Constants.buttonImageLeftInset
        button.imageEdgeInsets.top += Constants.buttonImageTopInset
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .regular)
        button.setTitle("Закрыть", for: .normal)
        button.addTarget(self, action: #selector(Self.close), for: .touchUpInside)
        button.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        button.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        button.setContentCompressionResistancePriority(.required, for: .vertical)
        button.isHidden = shouldHideCloseButton
        containerView.addSubview(button)
        closeButton = button
        
        NSLayoutConstraint.activate([
            titleImageTopConstraint,
            titleImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: Constants.horizontalSpacing),
            titleImageView.widthAnchor.constraint(equalToConstant: titleImageWidth),
            titleImageView.heightAnchor.constraint(equalToConstant: Constants.titleImageSideLength),
            
            title.topAnchor.constraint(equalTo: titleImageView.topAnchor, constant: Constants.titleTopOffset),
            title.leadingAnchor.constraint(equalTo: titleImageView.trailingAnchor, constant: titleSpacing),
            title.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -Constants.titleSpacing),
            
            button.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -Constants.horizontalSpacing),
            button.topAnchor.constraint(equalTo: title.topAnchor),
            
            handle.centerXAnchor.constraint(equalTo: draggable.centerXAnchor),
            handle.widthAnchor.constraint(equalToConstant: Constants.handleWidth),
            handle.heightAnchor.constraint(equalToConstant: Constants.handleHeight),
            handle.topAnchor.constraint(equalTo: draggable.topAnchor, constant: Constants.handleTopOffset),
            
            draggable.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            draggable.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            draggable.heightAnchor.constraint(equalToConstant: Constants.draggableHeight),
            draggable.topAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor),
            
            contentVC.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            contentVC.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            contentVCHeightConstraint,
            contentVCTopToTitleBottomConstraint,
            contentVC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - Public methods
extension SelectorViewController {
    @discardableResult
    public static func present(with viewController: UIViewController,
                               from presenter: UIViewController,
                               titleImage: UIImage? = nil,
                               isCloseButtonHidden shouldHideCloseButton: Bool = true,
                               mode: Mode = .requiredTopMargin,
                               background: BackgroundMode = .darkening,
                               panelBackground: BackgroundMode = .darkening,
                               completion: ((Bool) -> Void)? = nil) -> SelectorViewController {
        
        let svc = SelectorViewController(viewController: viewController)
        svc.shouldHideCloseButton = shouldHideCloseButton
        svc.titleImage = titleImage
        svc.mode = mode
        svc.backgroundMode = background
        svc.modalPresentationStyle = .custom
        if case .requiredTopMarginWithScrollView(let scrollView) = mode {
            let panGestureRecognizer = UIPanGestureRecognizer(target: svc, action: #selector(Self.pan(_:)))
            panGestureRecognizer.delegate = svc
            scrollView.addGestureRecognizer(panGestureRecognizer)
            svc.bottomDismissScrollViewPanGestureRecognizer = panGestureRecognizer
        }
        presenter.present(svc, animated: true) { }
        return svc
    }
    
    public func dismissAnimated(completion: (() -> Void)? = nil) {
        let completion = completion != nil ? completion : defaultCompletion
        presentingViewController?.dismiss(animated: true, completion: completion)
    }
}

// MARK: - Show/hide methods

extension SelectorViewController {
    @objc
    private func pan(_ gr: UIPanGestureRecognizer) {
        if let scrollViewGestureRecognizer = bottomDismissScrollViewPanGestureRecognizer,
           gr == scrollViewGestureRecognizer,
           case .requiredTopMarginWithScrollView(let scrollView) = mode {
            handlePan(state: gr.state, translation: gr.translation(in: scrollView))
        }
        return handlePan(state: gr.state, translation: gr.translation(in: draggableView))
    }
    
    private func handlePan(state: UIGestureRecognizer.State, translation: CGPoint) {
        switch state {
        case .possible:
            break
            
        case .began, .changed:
            let y: CGFloat
            if translation.y > 0 {
                y = translation.y
            } else {
                y = -min(pow(-translation.y, 0.65), Constants.verticalPanAllowance)
            }
            contentContainerView.transform = CGAffineTransform(translationX: 0.0, y: y)
            
        case .ended, .cancelled, .failed:
            if translation.y > min(Constants.verticalPanAllowance, blurredBackground.frame.height / 4) {
                dismissAnimated()
            } else {
                restorePosition()
            }
            
        @unknown default:
            break
        }
    }
    
    private func restorePosition() {
        UIView.animate(withDuration: Constants.resetAnimationDuration,
                       animations: {
                        self.contentContainerView.transform = .identity
                       },
                       completion: nil)
    }
    
    @objc
    private func tapOnBackground(_ gr: UITapGestureRecognizer) {
        if gr.location(in: contentContainerView).y < 0 {
            dismissAnimated()
        }
    }
    
    @objc
    private func close() {
        dismissAnimated()
    }
}

extension SelectorViewController {
    
    override public func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)
        switch mode {
        case .fullScreen:
            draggableView.isHidden = true
            closeButton.isHidden = shouldHideCloseButton
            titleImageTopConstraint?.constant = .zero
            
        case .requiredTopMargin, .requiredTopMarginWithScrollView:
            draggableView.isHidden = false
            closeButton.isHidden = true
            titleImageTopConstraint?.constant = Constants.verticalSpacing
            contentVCHeightConstraint?.constant = contentVC.preferredContentSize.height
        }
        
        updateTopConstraints()
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
}

// MARK: - UIGestureRecognizerDelegate

extension SelectorViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if gestureRecognizer == draggablePanGestureRecognizer {
            guard draggableView.bounds.contains(touch.location(in: draggableView)) else {
                return false
            }
            return !(touch.view is UIButton)
        }
        
        return true
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let scrollViewGestureRecognizer = bottomDismissScrollViewPanGestureRecognizer,
           gestureRecognizer == scrollViewGestureRecognizer,
           case .requiredTopMarginWithScrollView(let scrollView) = mode {
            let translation = scrollViewGestureRecognizer.translation(in: scrollView)
            if translation.y > .zero && scrollView.contentOffset.y <= .zero {
                return true
            }
            return false
        }
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if let scrollViewGestureRecognizer = bottomDismissScrollViewPanGestureRecognizer,
           gestureRecognizer === scrollViewGestureRecognizer,
           case .requiredTopMarginWithScrollView = mode {
            return true
        }
        return false
    }
}

// MARK: - Mode: Equatable

private extension SelectorViewController.Mode {
    static func == (lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case (.fullScreen, .fullScreen), (.requiredTopMargin, .requiredTopMargin):
            return true
            
        case (.requiredTopMarginWithScrollView(let lhsScrollView), .requiredTopMarginWithScrollView(let rhsScrollView)):
            return lhsScrollView === rhsScrollView
            
        default:
            return false
        }
    }
    
    var isFullScreen: Bool {
        return self == .fullScreen
    }
}

//MARK: - Transitioning and animations

extension SelectorViewController {
    fileprivate func clearBackground() {
        blurredBackground.effect = nil
        blurredBackground.backgroundColor = .clear
    }
    
    fileprivate func updateBackground() {
        switch backgroundMode {
        case .blured:
            blurredBackground.effect = UIBlurEffect(style: .regular)
            blurredBackground.backgroundColor = .clear
            contentContainerView.removeShadowWithBorder()
            
        case .darkening:
            blurredBackground.effect = nil
            blurredBackground.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            contentContainerView.removeShadowWithBorder()
            
        case .clear:
            blurredBackground.effect = nil
            blurredBackground.backgroundColor = .clear
            contentContainerView.addShadowWithBorder()
        }
    }
}

extension SelectorViewController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator = SelectorTransition(operation: .appearing, first: presenting, selector: self)
        return animator
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator = SelectorTransition(operation: .disappearing, first: presentingViewController, selector: self)
        return animator
    }
}

final class SelectorTransition: NSObject, UIViewControllerAnimatedTransitioning {
    enum Operation {
        case appearing
        case disappearing
    }
    private weak var firstViewController: UIViewController?
    private weak var secondViewController: SelectorViewController?
    private let operation: Operation
    
    private var keyboardTransitionTime: Double = 0
    private var keyboardHeight: CGFloat = .zero
    
    private let animationOptions: UIView.KeyframeAnimationOptions = {
        let animationOptions: UIView.AnimationOptions = .curveLinear
        return UIView.KeyframeAnimationOptions(rawValue: animationOptions.rawValue)
    }()
    
    init(operation: Operation, first: UIViewController?, selector: SelectorViewController) {
        self.firstViewController = first
        self.secondViewController = selector
        self.operation = operation
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        keyboardTransitionTime = 0.35
        keyboardTransitionTime = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        keyboardTransitionTime = 0.35
        keyboardTransitionTime = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//        transitionContext.
        0.35
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let presented = secondViewController else {
            return
        }
        presented.view.frame = transitionContext.finalFrame(for: presented)
        presented.view.layoutIfNeeded()
        transitionContext.containerView.addSubview(presented.view)
        
        if operation == .appearing {
            animateAppearence(transitionContext)
        } else {
            animateDisappiarence(transitionContext)
        }
    }
    
    private func animateAppearence(_ transitionContext: UIViewControllerContextTransitioning) {
        guard let presented = secondViewController else {
            return
        }
        presented.clearBackground()
        
        if keyboardTransitionTime == 0 {
            let duration = transitionDuration(using: transitionContext)
            let yOffset = presented.contentContainerView.frame.size.height
            presented.contentContainerView.transform = CGAffineTransform(translationX: 0.0, y: yOffset)
            UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseOut]) {
                presented.updateBackground()
                presented.contentContainerView.transform = .identity
            } completion: { bool in
                transitionContext.completeTransition(bool)
            }
        } else {
            let duration = max(transitionDuration(using: transitionContext), keyboardTransitionTime)
            UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseOut]) {
                presented.updateBackground()
            } completion: { bool in
                transitionContext.completeTransition(bool)
            }
            
            presented.contentContainerView.transform = CGAffineTransform(translationX: 0.0, y: 50)
            UIView.animate(withDuration: keyboardTransitionTime, delay: 0, options: [.curveEaseOut]) {
                presented.contentContainerView.transform = .identity
            }
            
            presented.contentContainerView.alpha = 0
            UIView.animate(withDuration: duration / 2, delay: 0, options: [.curveEaseOut]) {
                presented.contentContainerView.alpha = 1
            }
        }
    }
    
    private func animateDisappiarence(_ transitionContext: UIViewControllerContextTransitioning) {
        guard let presented = secondViewController else {
            return
        }
        
        if keyboardTransitionTime == 0 {
            let yOffset = presented.contentContainerView.frame.size.height
            let duration = transitionDuration(using: transitionContext)
            UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseOut]) {
                presented.clearBackground()
                presented.contentContainerView.transform = CGAffineTransform(translationX: 0.0, y: yOffset)
            } completion: { bool in
                transitionContext.completeTransition(bool)
            }
            
        } else {
            let duration = min(transitionDuration(using: transitionContext), keyboardTransitionTime)
            UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseOut]) {
                presented.clearBackground()
            } completion: { bool in
                transitionContext.completeTransition(bool)
            }
            
            let yOffset = presented.contentContainerView.frame.size.height
            UIView.animate(withDuration: keyboardTransitionTime, delay: 0, options: [.curveEaseOut]) {
                presented.contentContainerView.transform = CGAffineTransform(translationX: 0.0, y: yOffset)
            }
            
            UIView.animate(withDuration: duration/2, delay: duration/2, options: [.curveEaseOut]) {
                presented.contentContainerView.alpha = 0
            }
        }
    }
}
