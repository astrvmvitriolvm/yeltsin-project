//
//  BaseError.swift
//  Alcohelper
//
//  Created by Матвеев Олег on 25.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public enum BaseError: Error, LocalizedError {
    case generalError(description: String)
    
    public init(description: String) {
        self = .generalError(description: description)
    }
    
    public var errorDescription: String? {
        switch self {
        case let .generalError(description):
            return description
        }
    }
}
