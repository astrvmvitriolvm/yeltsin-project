//
//  SplashScreenRouter.swift
//  Alcohelper
//
//  Created by kogami on 13.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Costelique
import Model
import Base

public protocol AlcoListRouting {

    func showSettings(from: UIViewController)
    func showEdit(for: Drink, from: UIViewController)

}

public final class AppRouter: NSObject {

    private let context: DrinkContext
    private var tabBarController: TabBarController
    private var splashScreenDelay: Double = 0
    
    public init(context: DrinkContext) {
        self.context = context
        self.tabBarController = TabBarController.create()
        super.init()
        tabBarController.appRouter = self
        tabBarController.alcoList.router = self
        tabBarController.alcoList.viewModel = DrinkListViewModel(defaultsManager: context.defaultsManager, drinkManager: context.drinkManager)
        tabBarController.statistics.viewModel = StatisticsViewModel(defaultsManager: context.defaultsManager, alcoManager: context.drinkManager)
    }
    
    public func start(with window: inout UIWindow?, splashScreenDelay: Double = 0) {
        self.splashScreenDelay = splashScreenDelay
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = tabBarController
        window!.makeKeyAndVisible()
    }
    
    //tab bar routing
    func addDrink(from presenter: UIViewController) {
        if context.drinkManager.currentQueue == nil {
            show15minutesWarning(from: presenter)
        } else if context.drinkManager.warningLevel != .none {
            showAmountWarning(from: presenter)
        } else {
            showAddScreen(from: presenter)
        }
    }
    
    func presentSplashScreen(from viewController: UIViewController) {
        let splashScreen = SplashScreenVC.create(context: context, delay: splashScreenDelay)
        splashScreen.modalPresentationStyle = .overFullScreen
        splashScreen.modalTransitionStyle = .crossDissolve
        viewController.present(splashScreen, animated: false, completion: nil)
    }

    private func showAddScreen(from presenter: UIViewController) {
        let addViewController = AddDrinkController.create(context: context)
        addViewController.viewModel = AddDrinkViewModel(drinkManager: context.drinkManager)
        let timer = AKTimer()
        SelectorViewController.present(with: addViewController, from: presenter, background: .blured)
        timer.log("SelectorViewController.present")
    }

    private func show15minutesWarning(from presenter: UIViewController) {
        let alert = UIAlertController(title: "Еще не поздно",
                                      message: "Поставь таймер на 15 минут и подумай, действительно ли тебе это нужно",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ПИТЬ!!", style: .destructive, handler: { [weak self] action in
            self?.showAddScreen(from: presenter)
        }))
        alert.addAction(UIAlertAction(title: "Подумоть", style: .cancel))
        presenter.present(alert, animated: true)
    }

    private func showAmountWarning(from presenter: UIViewController) {
        switch context.drinkManager.warningLevel {
        case .none:
            return

        case .low:
            let alert = UIAlertController(title: "Пора притормозить",
                                          message: "Ты повеселился, дай отдохнуть организму",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ПИТЬ!!", style: .destructive, handler: { [weak self] action in
                self?.showAddScreen(from: presenter)
            }))
            alert.addAction(UIAlertAction(title: "Не пить", style: .cancel))
            presenter.present(alert, animated: true)

        case .high:
            let alert = UIAlertController(title: "Пора остановиться!",
                                          message: "Завтра будет очень плохо. Ты не скажешь себе спасибо",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ПИТЬ!!", style: .destructive, handler: { [weak self] action in
                self?.showAddScreen(from: presenter)
            }))
            alert.addAction(UIAlertAction(title: "Не пить", style: .cancel))
            presenter.present(alert, animated: true)

        case .extreme:
            let alert = UIAlertController(title: "Опасность!",
                                          message: "Если завтра не проснешься, пиняй на себя",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ПИТЬ!!", style: .destructive, handler: { [weak self] action in
                self?.showAddScreen(from: presenter)
            }))
            alert.addAction(UIAlertAction(title: "Не пить", style: .cancel))
            presenter.present(alert, animated: true)

        }
    }
}

extension AppRouter: AlcoListRouting {

    public func showEdit(for drink: Drink, from: UIViewController) {
        let vc = EditDrinkController.create()
        vc.viewModel = EditDrinkViewModel(drink: drink, drinkManager: context.drinkManager)
        from.present(vc, animated: true, completion: nil)
    }
    
    public func showSettings(from: UIViewController) {
        let vc = SettingsController.create(router: self, context: context)
        if let navigation = from.navigationController {
            navigation.pushViewController(vc, animated: true)
        } else {
            from.present(vc, animated: true, completion: nil)
        }
    }

}

extension AppRouter: SettingsRouter {

    func presentUserList(from vc: UIViewController) {
        let list = UserListController.create(with: context)
        vc.present(list, animated: true)
    }

}
