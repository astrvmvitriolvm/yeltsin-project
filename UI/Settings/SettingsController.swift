//
//  SettingsVC.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 03.08.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit
import Base

protocol SettingsRouter {
    func presentUserList(from: UIViewController)
}

final class SettingsController: BaseViewController {

    @IBOutlet private weak var weightField: UITextField!
    @IBOutlet private weak var genderSelection: UISegmentedControl!
    @IBOutlet private weak var genderField: UITextField!
    @IBOutlet private weak var hideAllSwitch: UISwitch!
    
    private var router: SettingsRouter!
    private var defaultsManager: DefaultsManaging!
    
    static func create(router: SettingsRouter, context: DrinkContext) -> SettingsController {
        let vc = UIStoryboard.main.instantiate(SettingsController.self)
        vc.router = router
        vc.defaultsManager = context.defaultsManager
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        weightField.delegate = self
        genderField.delegate = self
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideAllSwitch.isOn = !defaultsManager.hideAllExceptLast
        weightField.text = String(format: "%g", defaultsManager.weight)
        updateGender()
    }

    @IBAction func didChange(_ sender: UITextField) {
        switch sender {
        case weightField:
            if let value = weightField.doubleValue {
                defaultsManager.weight = min(610, max(20, value))
                weightField.text = String(format: "%g", defaultsManager.weight)
            }
            
        case genderField:
            if let text = genderField.text {
                defaultsManager.gender = .unknown(value: text)
            }
            
        default:
            break
        }
    }
    
    @IBAction func hideAllSwitchHandler(_ sender: Any) {
        defaultsManager.hideAllExceptLast = !hideAllSwitch.isOn
    }
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        switch sender {
        case genderSelection:
            if genderSelection.selectedSegmentIndex == 0 {
                genderField.isHidden = true
                defaultsManager.gender = .male
            } else if genderSelection.selectedSegmentIndex == 1 {
                genderField.isHidden = true
                defaultsManager.gender = .female
            } else if genderSelection.selectedSegmentIndex == 2 {
                genderField.isHidden = false
                defaultsManager.gender = .unknown(value: genderField.text ?? "")
            }
        default:
            break
        }
    }
    
    private func updateGender() {
        switch defaultsManager.gender {
        case .male:
            genderSelection.selectedSegmentIndex = 0
            genderField.isHidden = true
        case .female:
            genderSelection.selectedSegmentIndex = 1
            genderField.isHidden = true
        case .unknown(let value):
            genderSelection.selectedSegmentIndex = 2
            genderField.isHidden = false
            genderField.text = value
        }
    }
    
    @IBAction func changeUserAction(_ sender: Any) {
        view.endEditing(true)
        router.presentUserList(from: self)
    }
}

extension SettingsController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

fileprivate extension UITextField {
    var doubleValue: Double? {
        if let text = self.text {
            let formatted = text.replacingOccurrences(of: ",", with: ".")
            if let value = Double(formatted) {
                return value
            }
        }
        return nil
    }
}
