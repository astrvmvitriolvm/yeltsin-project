//
//  UserListController.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 22.04.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Base
import Model
import Costelique

class UserListController: UIViewController {
    private enum Constants {
        static let newUser = "Создать нового"
    }
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var userNameField: UITextField!
    private var defaultsManager: DefaultsManaging!
    private var context: DrinkContext!
    
    static func create(with context: DrinkContext) -> Self {
        let vc = self.instantinateFromNib()
        vc.defaultsManager = context.defaultsManager
        vc.context = context
        return vc
    }
    
    var list: [Drunkard] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let name = defaultsManager.name
        userNameField.text = name
        list = defaultsManager.allUsers
        setupTableView()
        tableView.reloadData()
    }
    
    private func setupTableView() {
        tableView.registerClass(forCell: UserNameCell.self)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func nameChanged(_ sender: Any) {
        defaultsManager.name = userNameField.text ?? ""
    }
    
    private func deleteUser(name: String) {
        var users = defaultsManager.allUsers
        guard let user = users.first(where: { $0.name == name }) else {
            return
        }
        
        let deleteHandler = {
            users.removeAll(where: { $0.name == name })
            self.defaultsManager.allUsers = users
            self.list = users
            self.tableView.reloadData()
        }
        
        guard user.drinks.count == 0 else {
            let alert = UIAlertController(title: "Подтвердите удаление", message: "Это приведет к потере всей истории пользователя \(user.name)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Отменть", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { _ in
                deleteHandler()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        deleteHandler()
    }
}
 
extension UserListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(UserNameCell.self, for: indexPath)
        if list.indices.contains(indexPath.item) {
            cell.setup(name: list[indexPath.item].name)
        } else {
            cell.setup(name: Constants.newUser)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        guard defaultsManager.name.count > 0 || defaultsManager.drinks.count == 0 else {
            let alert = UIAlertController(title: "Введите имя текущего пользователя", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OК", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        guard !list.contains(where: { $0.name == defaultsManager.name }) else {
            let alert = UIAlertController(title: "Пользователь с таким именем уже существует", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OК", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if list.indices.contains(indexPath.item) {
            context.changeUser(with: list[indexPath.item].name)
        } else {
            context.changeUser(with: "")
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if let user = list[safe: indexPath.item] {
            let deleteItem = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, _) in
                self?.deleteUser(name: user.name)
            }
            return UISwipeActionsConfiguration(actions: [deleteItem])
        }
        return nil
    }
}


fileprivate class UserNameCell: UITableViewCell {
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            contentView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    func setup(name: String) {
        label.text = name
    }
}
