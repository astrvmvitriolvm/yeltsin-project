//
//  SplashScreenVC.swift
//  Alcohelper
//
//  Created by kogami on 13.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Model
import Base

final class SplashScreenVC: UIViewController {
    
    static func create(context: DrinkContext, delay: Double = 0) -> SplashScreenVC {
        let vc = UIStoryboard.main.instantiate(SplashScreenVC.self)
        vc.defaultsManager = context.defaultsManager
        vc.drinkManager = context.drinkManager
        vc.delay = delay
        return vc
    }
    
    @IBOutlet private weak var blackSun: UIImageView!
    @IBOutlet private weak var eltsin: UIImageView!
    private var delay: Double = 0
    
    var defaultsManager: DefaultsManaging!
    var drinkManager: DrinkManager!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateBlackSun(duration: 6)
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.drinkManager.isLoading.addObserver(self) { strongSelf, loading in
                if !loading {
                    DispatchQueue.main.async {
                        strongSelf.dismiss()
                    }
                }
            }
        }
    }
    
    private func dismiss() {
        defaultsManager!.isFirstLaunch = false
        dismiss(animated: true, completion: nil)
    }
    
    private func animateBlackSun(duration: Double) {
        
        let setupAnimation: (CABasicAnimation) -> Void = { animation in
            animation.duration = duration
            animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            animation.autoreverses = true
            animation.repeatCount = 100
            animation.fillMode = .forwards
        }
        
        blackSun.layer.add({
            let animation = CABasicAnimation(keyPath: "transform.rotation.z")
            setupAnimation(animation)
            animation.fromValue = 0
            animation.toValue = -CGFloat.pi / 2
            animation.autoreverses = false
            return animation
        }(), forKey: "angle")
        
        blackSun.layer.add({
            let animation = CABasicAnimation(keyPath: "transform.scale")
            setupAnimation(animation)
            animation.fromValue = 1
            animation.toValue = 0.9
            return animation
        }(), forKey: "scale")
        
        eltsin.layer.add({
            let animation = CABasicAnimation(keyPath: "transform.scale")
            setupAnimation(animation)
            animation.fromValue = 1
            animation.toValue = 1.2
            return animation
        }(), forKey: "scale")
    }
}
