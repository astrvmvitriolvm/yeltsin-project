//
//  TabBarController.swift
//  Alcohelper
//
//  Created by kogami on 13.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Costelique

final class TabBarController: UITabBarController {
    
    static func create() -> TabBarController {
        UIStoryboard.main.instantiate(TabBarController.self)
    }
    
    let alcoList: DrinkListController = {
        let vc = UIStoryboard.main.instantiate(DrinkListController.self)
        vc.tabBarItem = UITabBarItem(title: "Бухлишко", image: UIImage(named: "beer"), tag: 0)
        return vc
    }()
    
    let statistics: StatisticsController = {
        let vc = UIStoryboard.main.instantiate(StatisticsController.self)
        vc.tabBarItem = UITabBarItem(title: "Статистика", image: UIImage(named: "bar-chart"), tag: 2)
        return vc
    }()
    
    var appRouter: AppRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        viewControllers = [
            UINavigationController(rootViewController: alcoList),
            UIViewController(),
            UINavigationController(rootViewController: statistics)
        ]
        setupMiddleButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appRouter?.presentSplashScreen(from: self)
    }
    
    private func setupMiddleButton() {
        let size: CGFloat = 45
        let menuButton = AddButton()
        menuButton.translatesAutoresizingMaskIntoConstraints = false
        menuButton.addTarget(self, action: #selector(plusButtonAction), for: .touchUpInside)
        view.addSubview(menuButton)
        
        NSLayoutConstraint.activate([
            menuButton.widthAnchor.constraint(equalToConstant: size),
            menuButton.heightAnchor.constraint(equalToConstant: size),
            menuButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            menuButton.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
        ])
    }
    
    @objc
    private func plusButtonAction() {
        appRouter?.addDrink(from: self)
    }
}

extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        viewController is UINavigationController
    }
}
