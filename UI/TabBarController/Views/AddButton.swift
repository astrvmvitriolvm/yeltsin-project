//
//  AddButton.swift
//  Alcohelper
//
//  Created by kogami on 20.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit

final class AddButton: UIButton {
    private let backgroundLayer: CALayer = CALayer()
    
    init() {
        super.init(frame: .zero)
        adjustsImageWhenHighlighted = false
        backgroundLayer.backgroundColor = UIColor.systemBlue.cgColor
        backgroundLayer.shadowColor = UIColor.systemBlue.cgColor
        backgroundLayer.shadowOpacity = 0.2
        backgroundLayer.shadowRadius = 5
        backgroundLayer.shadowOffset = CGSize(width: 0, height: 5)
        backgroundLayer.zPosition = -1
        
        layer.insertSublayer(backgroundLayer, at: 0)
        setImage(UIImage(named: "add"), for: .normal)
        
        addTarget(self, action: #selector(touched), for: .touchDown)
        addTarget(self, action: #selector(touched), for: .touchDragEnter)
        addTarget(self, action: #selector(untouched), for: .touchDragExit)
        addTarget(self, action: #selector(untouched), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isHighlighted {
            return
        }
        backgroundLayer.frame = bounds
        backgroundLayer.cornerRadius = bounds.height / 2
        backgroundLayer.shadowPath = CGPath(ellipseIn: bounds, transform: nil)
    }
    
    @objc private func touched() {
        let scale: CGFloat = 0.95
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.1)
        backgroundLayer.transform = CATransform3DMakeScale(scale, scale, 1)
        backgroundLayer.backgroundColor = UIColor.systemBlue.withAlphaComponent(0.9).cgColor
        CATransaction.commit()
    }
    
    @objc private func untouched() {
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.1)
        backgroundLayer.transform = CATransform3DIdentity
        backgroundLayer.backgroundColor = UIColor.systemBlue.cgColor
        CATransaction.commit()
    }
}
