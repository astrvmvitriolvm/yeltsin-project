//
//  StatisticsViewModel.swift
//  Alcohelper
//
//  Created by kogami on 20.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Base
import Model

struct StatisticsSection {
    let title: String
    let parameters: [StatisticsParameter]
}

enum StatisticsParameter {
    case stat(StaisticsDefaultCellViewModel)
    case chart(StatisticsChartViewModel)
}

protocol StatisticsViewModeling {
    func refresh()
    var updateHandler: (()->Void)? { get set }
    
    func sectionsCount() -> Int
    func sectionTitle(_ int: Int) -> String?
    func rowsInSection(_ int: Int) -> Int
    func viewModel(for: IndexPath) -> StatisticsParameter
}

final class StatisticsViewModel {
    private let defaultsManager: DefaultsManaging
    private let drinkManager: DrinkManager
    var updateHandler: (() -> Void)?

    private var sections: [StatisticsSection] = []
        
    init(defaultsManager: DefaultsManaging, alcoManager: DrinkManager) {
        self.defaultsManager = defaultsManager
        self.drinkManager = alcoManager
        recalculate()
    }
    
    func refresh() {
        recalculate()
    }
    
    func sectionsCount() -> Int {
        sections.count
    }
    
    func sectionTitle(_ int: Int) -> String? {
        sections[safe: int]?.title
    }
    
    private func recalculate() {
        sections = [
            makeStatSection(),
            getTodaySection(),
            getWeekSection()
        ].filter { !$0.parameters.isEmpty }
        updateHandler?()
    }
    
    private func makeStatSection() -> StatisticsSection {
//        let selectedPeriod
        let chart = StatisticsParameter.chart(StatisticsChartViewModel(defaultsManager: defaultsManager, drinkManager: drinkManager))
        let section = StatisticsSection(title: "График", parameters: [chart])
        return section
    }
    
    private func getTodaySection() -> StatisticsSection {
        let ml = drinkManager.millilitersInBlood(at: Date())
        let (mlMax, dateMax) = (drinkManager.currentQueue?.expectedMaximum()).map({ ($0.value, $0.date) }) ?? (0, nil)
        
        let total = drinkManager.millilitersInRow(at: Date())
        
        var today: [StatisticsParameter] = [
            .stat(.init(title: "Всего выпито", value: " \(total) мг"))
            ]
        
        if ml > 0 {
            today.append(.stat(.init(title: "Этанола в крови", value: "\(ml) мг")))
        }
        if mlMax > ml, let date = dateMax {
            let maxDate = "в \(DateFormatter.timeAndDateIfToday(date))"
            today.append(.stat(.init(title: "Ожидаемый максимум", value: "\(mlMax) мг", substat: maxDate)))
        }
        if mlMax > 0, let detoxTime = drinkManager.detoxicationTime(at: Date()) {
            let minutesLeft = Int(detoxTime.timeIntervalSinceNow) / 60
            let timeFormatted = "\(minutesLeft / 60) ч. \(minutesLeft % 60) м."
            let detoxString = "в \(DateFormatter.timeAndDateIfToday(detoxTime))"
            today.append(.stat(.init(title: "Время до выведения", value: timeFormatted, substat: detoxString)))
        }
        
        return StatisticsSection(title: "Сегодня", parameters: today)
    }
    
    private func getWeekSection() -> StatisticsSection {
        let pointsAtThisWeek = drinkManager.pointsOfAlcoholInThisWeek()
        var week: [StatisticsParameter] = []
        if pointsAtThisWeek > 0 {
            let points = (pointsAtThisWeek * 10).rounded() / 10
            week.append(.stat(.init(title: "Выпито", value: "\(points) ед. алкоголя", substat: "не больше 10")))
        }
        return StatisticsSection(title: "В течении недели", parameters: week)
    }
}

// - StatisticsViewModeling
extension StatisticsViewModel: StatisticsViewModeling {
    
    func rowsInSection(_ int: Int) -> Int {
        sections[int].parameters.count
    }
    
    func viewModel(for indexPath: IndexPath) -> StatisticsParameter {
        sections[safe: indexPath.section]?.parameters[safe: indexPath.item] ?? .stat(.empty)
    }
    
}
