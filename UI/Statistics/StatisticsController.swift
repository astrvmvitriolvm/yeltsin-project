//
//  StatisticsVC.swift
//  Alcohelper
//
//  Created by kogami on 13.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Costelique

final class StatisticsController: BaseViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: StatisticsViewModeling? {
        didSet {
            updateView()
            viewModel?.updateHandler = { [weak self] in
                self?.updateView()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerNib(forCell: StatisticsDefaultCell.self)
        tableView.registerNib(forCell: StatisticsChartCell.self)
        tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.refresh()
    }
    
    private func updateView() {
        guard isViewLoaded else {
            return
        }
        tableView.reloadData()
    }
}

extension StatisticsController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel?.sectionsCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel?.sectionTitle(section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.rowsInSection(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = viewModel?.viewModel(for: indexPath) else {
            return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        switch item {
        case .stat(let vm):
            let cell = tableView.dequeue(StatisticsDefaultCell.self, for: indexPath)
            cell.configure(with: vm)
            return cell
            
        case .chart(let vm):
            let cell = tableView.dequeue(StatisticsChartCell.self, for: indexPath)
            cell.configure(with: vm)
            return cell
        }
    }
}
