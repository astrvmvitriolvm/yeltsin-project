//
//  StatisticsDefaultCell.swift
//  Alcohelper
//
//  Created by kogami on 20.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit

struct StaisticsDefaultCellViewModel {
    let title: String
    let value: String
    let substat: String?
    
    init(title: String, value: String, substat: String? = nil) {
        self.title = title
        self.value = value
        self.substat = substat
    }
    
    static var empty: StaisticsDefaultCellViewModel {
        StaisticsDefaultCellViewModel(title: "", value: "")
    }
}

final class StatisticsDefaultCell: UITableViewCell {
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblValue: UILabel!
    
    @IBOutlet private weak var substatContainer: UIView!
    @IBOutlet private weak var lblSubstat: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        lblValue.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        lblSubstat.font = UIFont.systemFont(ofSize: 12, weight: .light)
        lblSubstat.textAlignment = .right
        selectionStyle = .none
    }

    func configure(with vm: StaisticsDefaultCellViewModel) {
        lblTitle.text = vm.title
        lblValue.text = vm.value
        
        substatContainer.isHidden = vm.substat?.isEmpty ?? true
        lblSubstat.text = vm.substat
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
