//
//  StatisticsGraphCell.swift
//  UI
//
//  Created by Матвеев Олег on 02.11.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Model

class StatisticsChartCell: UITableViewCell {

    private var viewModel: StatisticsChartViewModel?
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var chartView: ChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with viewModel: StatisticsChartViewModel) {
        self.viewModel = viewModel
        segmentControl.selectedSegmentIndex = viewModel.selectedPeriod.rawValue
        viewModel.data.addObserver(self) { weakSelf, data in
            var normal: Float = 0
            if let period = weakSelf.viewModel?.selectedPeriod {
                switch period {
                case .day: normal = 28.57
                case .week: normal = 200
                case .month: normal = 869
                case .year: normal = 10_435.71
                }
            }

            weakSelf.chartView.configure(data, normal: normal)
        }
    }
    
    @IBAction func segmentAction(_ sender: Any) {
        viewModel?.selectedPeriod = TimePeriod.allCases[segmentControl.selectedSegmentIndex]
    }
}


class ChartView: UIView {
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private var graphLayer = CAShapeLayer()
    private var highlightedLayer = CAShapeLayer()
    private var normalSign = CAShapeLayer()
    private var data: [ChartData] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        addSubview(scrollView)
        layer.addSublayer(graphLayer)
        layer.addSublayer(highlightedLayer)
        layer.addSublayer(normalSign)
    }
    
    override func layoutSubviews() {
        graphLayer.frame = layer.bounds
        updateChart()
    }
    
    func configure(_ data: [ChartData], normal: Float) {
        self.data = data
        updateChart(normal: normal)
    }
    
    private func updateChart(normal: Float = 0) {
        guard data.count > 0 else {
            return
        }
        let maxVal = data.reduce(0, { max($0, $1.value) })
        let margins: CGFloat = 8
        let chartLength = graphLayer.frame.width - margins * 2
        var valueLength = chartLength / CGFloat(data.count)
        let space = valueLength * 0.1
        valueLength *= 0.9
        
        let path = UIBezierPath()
        let hPath = UIBezierPath()

        let lastIndex = data.count - 1
        for (index, period) in data.enumerated() {
            let x = valueLength * CGFloat(lastIndex - index) + space * CGFloat(lastIndex - index) + margins
            let height = CGFloat(period.value / maxVal) * graphLayer.frame.height
            let y = graphLayer.frame.height - height
            let rect = CGRect(x: x, y: y, width: valueLength, height: height)
            let newPath = UIBezierPath(rect: rect)
            (period.isHighlighted ? hPath : path).append(newPath)
        }
        
        graphLayer.fillColor = UIColor.gray.cgColor
        graphLayer.path = path.cgPath

        highlightedLayer.fillColor = UIColor.lightGray.cgColor
        highlightedLayer.path = hPath.cgPath

        if normal != 0 {
            addNormalLine(y: graphLayer.frame.height * (1 - CGFloat(normal / maxVal)))
        }
    }

    private func addNormalLine(y: CGFloat) {
        let path = UIBezierPath()
        let margins: CGFloat = 8
        let chartLength = graphLayer.frame.width - margins
        path.move(to: CGPoint(x: margins, y: y))
        path.addLine(to: CGPoint(x: chartLength, y: y))

        normalSign.lineWidth = 1
        normalSign.strokeColor = UIColor.red.cgColor
        normalSign.path = path.cgPath
    }
}
