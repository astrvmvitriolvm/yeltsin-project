//
//  StatisticsCellViewModel.swift
//  UI
//
//  Created by Матвеев Олег on 02.11.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Model
import Base
import UIKit
import Costelique

class StatisticsChartViewModel {

    let defaultsManager: DefaultsManaging
    let drinkManager: DrinkManager
    
    var selectedPeriod: TimePeriod  {
        get {
            defaultsManager.selectedChartPeriod
        }
        set {
            defaultsManager.selectedChartPeriod = newValue
            updateData()
        }
    }
    
    var data: Observable<[ChartData]> = .init([])
    
    init(defaultsManager: DefaultsManaging, drinkManager: DrinkManager) {
        self.defaultsManager = defaultsManager
        self.drinkManager = drinkManager
        updateData()
    }
    
    func updateData() {
        data.wrappedValue = drinkManager.getDataBy(period: selectedPeriod)
    }
}
