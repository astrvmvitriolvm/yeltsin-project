//
//  ViewController.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 23.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit
import Model

final class DrinkListController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var warningLayer: RadialGradientLayer = {
        let layer = RadialGradientLayer()
        layer.isHidden = true
        let vc = self.navigationController?.tabBarController ?? self
        layer.frame = vc.view.bounds
        vc.view.layer.addSublayer(layer)
        return layer
    }()
        
    var router: AlcoListRouting?
    
    var viewModel: DrinkListViewModel? {
        didSet {
            updateView()
            viewModel?.updateHandler = { [weak self] in
                self?.updateView()
            }
        }
    }
    
    //MARK: Lifecycle and appearence
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.updateDefaults()
    }
    
    private var timer: Timer?
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateWarning()
        timer = Timer(timeInterval: 2, repeats: true) { [weak self] _ in
            guard let strongSelf = self, strongSelf.isVisible else {
                return
            }
            strongSelf.updateWarning()
            for cell in strongSelf.tableView.visibleCells {
                if let alcoCell = cell as? DrinkCell {
                    alcoCell.updateProgress()
                } else if let graphCell = cell as? GraphCell {
                    graphCell.update()
                } else if let headerCell = cell as? DrinkHeaderCell {
                    headerCell.update()
                }
            }
        }
        RunLoop.current.add(timer!, forMode: .common)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc private func appMovedToForeground() {
        animateWarning()
    }
    
    //MARK: View and layout
    
    private func setupView() {
        tableView.registerNib(forCell: DrinkCell.self)
        tableView.registerNib(forCell: GraphCell.self)
        tableView.registerNib(forCell: DrinkHeaderCell.self)
        tableView.tableFooterView = UIView()
    }
    
    private func updateView() {
        guard isViewLoaded else {
            return
        }
        self.tableView.reloadData()
    }
    
    private func updateWarning() {
        guard let viewModel = viewModel else {
            return
        }
        switch viewModel.warningLevel {
        case .none:
            warningLayer.isHidden = true
            
        case .low:
            warningLayer.isHidden = false
            warningLayer.colors = [UIColor.red.withAlphaComponent(0), UIColor.red.withAlphaComponent(0.4)]
            
        case .high:
            warningLayer.isHidden = false
            warningLayer.colors = [UIColor.red.withAlphaComponent(0), UIColor.red]

        case .extreme:
            warningLayer.isHidden = false
            warningLayer.colors = [UIColor.red.withAlphaComponent(0.4), UIColor.red]
        }
        animateWarning()
    }
    
    private func animateWarning() {
        let animation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 1
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.repeatCount = 900000
        animation.autoreverses = true
        animation.fillMode = .forwards
        warningLayer.add(animation, forKey: "fade")
    }
    
    private func edit(_ drink: Drink) {
        view.endEditing(true)
        router?.showEdit(for: drink, from: self)
    }
    
    @IBAction func settingsAction(_ sender: Any) {
        router?.showSettings(from: self)
    }
}

extension DrinkListController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        53
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel?.sectionsCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.cellsInSection(section) ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return UITableViewCell(frame: .zero)
        }

        if indexPath.section == viewModel.sectionsCount - 1,
           indexPath.item == viewModel.cellsInSection(indexPath.section) - 1 {
            DispatchQueue.main.async {
                self.viewModel?.loadNextPage()
            }
        }
        
        let element = viewModel.element(at: indexPath)
        switch element {
        case .drinkCell(let viewModel):
            let cell = tableView.dequeue(DrinkCell.self, for: indexPath)
            cell.configure(model: viewModel)
            cell.updateProgress()
            cell.doubleTapHandler = { [weak self] in
                guard let strongSelf = self, let drink = $0.representedObject else {
                    return
                }
                strongSelf.edit(drink)
            }
            return cell
            
        case .graphCell(let viewModel):
            let cell = tableView.dequeue(GraphCell.self, for: indexPath)
            cell.configure(with: viewModel)
            return cell
            
        case .headerCell(let viewModel):
            let cell = tableView.dequeue(DrinkHeaderCell.self, for: indexPath)
            cell.configure(viewModel)
            return cell
        }
    }

     func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if let cell = tableView.cellForRow(at: indexPath) as? DrinkCell {
            let editItem = UIContextualAction(style: .normal, title: "Edit") { [weak self] (_, _, _) in
                if let drink = cell.representedObject {
                    self?.edit(drink)
                }
            }
            let deleteItem = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, _) in
                if let drink = cell.representedObject {
                    self?.viewModel?.remove(drink: drink)
                }
            }
            return UISwipeActionsConfiguration(actions: [deleteItem, editItem])
        }
        return nil
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel?.headerForSection(section)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
