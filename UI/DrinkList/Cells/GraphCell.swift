//
//  GraphCell.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 27.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Model
import Costelique

struct GraphCellViewModel {
    let graphData: GraphData
}

class GraphCell: UITableViewCell {
    private let graphHeight: CGFloat = 100
    private let graphInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
    
    @IBOutlet private weak var lblStart: UILabel!
    @IBOutlet private weak var lblEnd: UILabel!
    @IBOutlet private weak var graphView: UIView!
    @IBOutlet private weak var maxValueLabel: UILabel!
    @IBOutlet private weak var maxDateLabel: UILabel!
    
    private let gridLayer = CAShapeLayer()
    private let shapeLayer = CAShapeLayer()
    private let dateLayer = CAShapeLayer()
    private var viewModel: GraphCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        graphView.layer.addSublayer(gridLayer)
        graphView.layer.addSublayer(shapeLayer)
        graphView.layer.addSublayer(dateLayer)
    }
    
    func configure(with viewModel: GraphCellViewModel) {
        self.viewModel = viewModel
        let formatter: DateFormatter = viewModel.start.isSameDay(viewModel.end) ? .timeFormatter : .timeAndDateFormatter
        lblStart.text = formatter.string(from: viewModel.start)
        lblEnd.text = formatter.string(from: viewModel.end)
        createGrid()
        createGraph()
        updateDateLayer()
    }
    
    func update() {
        updateDateLayer()
    }
    
    private func createGrid() {
        guard let viewModel = self.viewModel else {
            return
        }
        let leftInset = graphInsets.left
        let width = graphView.bounds.width - graphInsets.left - graphInsets.right
        let height = self.graphHeight - graphInsets.bottom - graphInsets.top
        var maxValue = viewModel.maximumValue
        if maxValue < 10 {
            maxValue = 10
        }
        let makeHorisontalLine: ((Float) -> CGFloat) = { value in
            height - height * CGFloat(value / maxValue)
        }
        
        let path = UIBezierPath()
        
        for i in 0 ... Int(maxValue / 10) + 1 {
            let lineHeigh = makeHorisontalLine(Float(i * 10))
            path.move(to: CGPoint(x: leftInset, y: lineHeigh))
            path.addLine(to: CGPoint(x: leftInset + width, y: lineHeigh))
        }
        gridLayer.lineWidth = 1
        gridLayer.path = path.cgPath
        gridLayer.strokeColor = UIColor.systemGray.withAlphaComponent(0.5).cgColor
        gridLayer.fillColor = UIColor.clear.cgColor
        maxValueLabel.text = "\(Int(viewModel.maximumValue)) мл"
        maxDateLabel.text = ""//Максимум " + DateFormatter.timeAndDateIfToday(viewModel.maximumDate)
    }
    
    
    private func createGraph() {
        guard let viewModel = self.viewModel, viewModel.end > viewModel.start else {
            return
        }
        let leftInset = graphInsets.left
        let width = graphView.bounds.width - graphInsets.left - graphInsets.right
        let height = self.graphHeight - graphInsets.bottom - graphInsets.top
        let points = viewModel.graphData.points
        var maxValue = viewModel.maximumValue
        if maxValue < 10 {
            maxValue = 10
        }
        
        let length = viewModel.end.timeIntervalSince(viewModel.start)
        let makePoint: ((Date, Float) -> CGPoint) = { date, value in
            let x = leftInset + CGFloat(date.timeIntervalSince(viewModel.start) / length) * width
            let y = height - height * CGFloat(value / maxValue)
            return CGPoint(x: x, y: y)
        }
        
        let path = UIBezierPath()
        path.move(to: makePoint(viewModel.start,0))
        for point in points {
            let point = makePoint(point.date, point.value)
            path.addLine(to: point)
        }
        shapeLayer.lineWidth = 2
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.systemBlue.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
    }
    
    private func updateDateLayer() {
        let date = Date()
        guard let viewModel = viewModel, date > viewModel.start, date < viewModel.end else {
            dateLayer.isHidden = true
            return
        }
        dateLayer.isHidden = false
        let leftInset = graphInsets.left
        let width = graphView.bounds.width - graphInsets.left - graphInsets.right
        
        let progress = date.timeIntervalSince(viewModel.start) / viewModel.end.timeIntervalSince(viewModel.start)
        let x = leftInset + width * CGFloat(progress)
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: x, y: 0))
        path.addLine(to: CGPoint(x: x, y: graphHeight))
        dateLayer.path = path.cgPath
        dateLayer.fillColor = UIColor.systemRed.cgColor
        dateLayer.strokeColor = UIColor.alizarine.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.createGrid()
            self.createGraph()
            self.updateDateLayer()
        }
    }
}

extension UIColor {
    static var alizarine: UIColor {
        UIColor(red: 255/255, green: 50/255, blue: 50/255, alpha: 1)
    }
}

private extension GraphCellViewModel {
    var start: Date {
        graphData.points.first?.date ?? Date()
    }
    
    var end: Date {
        graphData.points.last?.date ?? Date()
    }
    
    var maximumValue: Float {
        graphData.points.reduce(0, { max($0, $1.value) })
    }
    
    var maximumDate: Date {
        var max: Float = 0
        var maxDate = Date()
        for point in graphData.points {
            if point.value > max {
                maxDate = point.date
                max = point.value
            }
        }
        return maxDate
    }
}
