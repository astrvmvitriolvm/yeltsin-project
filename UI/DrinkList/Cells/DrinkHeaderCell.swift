//
//  DrinkHeaderCell.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 04.04.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit

struct DrinkHeaderViewModel {
    struct Parameter {
        let title: String
        let value: String
    }
    let parameters: () -> [Parameter]
    let isCurrentQueue: () -> Bool
}

class DrinkHeaderCell: UITableViewCell {
    private enum Constants {
        static let verticalSpacing: CGFloat = 4
    }
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    private var viewModel: DrinkHeaderViewModel?
    
    func configure(_ viewModel: DrinkHeaderViewModel) {
        self.viewModel = viewModel
        updateStackView()
    }
    
    private func makeParameterView(_ parameter: DrinkHeaderViewModel.Parameter) -> UIView {
        let view = UIView()
        
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        title.text = parameter.title
        view.addSubview(title)
        
        let value = UILabel()
        value.translatesAutoresizingMaskIntoConstraints = false
        value.font = UIFont.systemFont(ofSize: 13, weight: .light)
        value.text = parameter.value
        view.addSubview(value)
        
        NSLayoutConstraint.activate([
            title.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            title.topAnchor.constraint(equalTo: view.topAnchor, constant: Constants.verticalSpacing),
            title.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Constants.verticalSpacing),
            
            value.leadingAnchor.constraint(equalTo: title.trailingAnchor, constant: 10),
            value.topAnchor.constraint(equalTo: view.topAnchor, constant: Constants.verticalSpacing),
            value.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Constants.verticalSpacing),
        ])
        
        return view
    }
    
    func update() {
        guard let viewModel = viewModel, viewModel.isCurrentQueue() else {
            return
        }
        updateStackView()
    }
    
    private func updateStackView() {
        stackView.removeAllSubviews()
        guard let viewModel = viewModel else {
            return
        }
        viewModel.parameters().forEach {
            stackView.addArrangedSubview(makeParameterView($0))
        }
    }
}
