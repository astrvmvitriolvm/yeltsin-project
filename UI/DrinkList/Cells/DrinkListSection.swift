//
//  AlcoListSections.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 27.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Model

enum DrinkListElement {
    case drinkCell(DrinkCellViewModel)
    case graphCell(GraphCellViewModel)
    case headerCell(DrinkHeaderViewModel)
}

class DrinkListSection {
    private let queue: DrinkQueuing
    
    var count: Int {
        queue.count + 2
    }
    
    lazy var cells: [DrinkListElement] = {
        let headerViewModel = makeHeaderCell()
        let graphicViewModel = GraphCellViewModel(graphData: queue.getTimeline())
        let oneDay = queue.first.date.isSameDay(queue.last.date)
        
        var elements: [DrinkListElement] = [
            .headerCell(headerViewModel),
            .graphCell(graphicViewModel)
        ]
        elements.append(contentsOf: queue.getAllDrinks().reversed().map {
            .drinkCell(DrinkCellViewModel(drink: $0, showDate: !oneDay, progress: makeProgressBlockFor($0)))
        })
        return elements
    }()
    
    lazy var title: String = {
        let first = queue.first.date
        let last = queue.last.date
        
        var dateString = DateFormatter.dayMonthLong.string(from: first)
        if !first.isSameDay(last) {
            dateString += " - " + DateFormatter.dayMonthLong.string(from: last)
        }
        if !last.isSameYear(Date()) {
            dateString += " " + DateFormatter.year.string(from: last)
        }
        return dateString
    }()
    
    init (queue: DrinkQueuing) {
        self.queue = queue
    }
    
    private func makeProgressBlockFor(_ drink: Drink) -> () -> Float {
        return { [weak self, weak drink] in
            guard let strongSelf = self else {
                print("WARNING: DrinkListSection.cells progress calls on deinitialized section")
                return 1
            }
            guard let drink = drink else {
                print("WARNING: DrinkListSection.cells progress calls on deinitialized drink")
                return 1
            }
            return strongSelf.queue.detoxicationProgress(of: drink, at: Date())
        }
    }
    
    private func makeHeaderCell() -> DrinkHeaderViewModel {
        let maximum = queue.expectedMaximum()
        
        var maxDate = DateFormatter.timeAndDateIfToday(maximum.date)
        if maximum.date.isSameDay(Date()) {
            maxDate = "в " + maxDate
        }
        
        let parameters: () -> [DrinkHeaderViewModel.Parameter] = { [weak self] in
            guard let queue = self?.queue else {
                return []
            }
            var parameters: [DrinkHeaderViewModel.Parameter] = [
                .init(title: "Всего выпито \(Int(queue.milliliters.rounded())) мл", value: ""),
                .init(title: "Максимум в крови \(Int(maximum.value.rounded())) мл", value: maxDate),
            ]
            if queue.isCurrent {
                let ml = queue.millilitersInBlood(at: Date()).rounded()
                parameters.append(.init(title: "Сейчас \(Int(ml)) мл", value: ""))
            }
            return parameters
        }
        
        let isCurrent: () -> Bool = { [weak self] in
            self?.queue.isCurrent ?? false
        }
        
        return DrinkHeaderViewModel(parameters: parameters, isCurrentQueue: isCurrent)
    }
}
