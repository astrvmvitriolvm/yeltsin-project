//
//  GraphicView.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 04.05.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Costelique

struct GraphicPoint {
    let x: Float
    let y: Float
    
    let xDescription: String?
    let yDescription: String?
}

class GraphicViewModel {
    var points: [GraphicPoint] = []
}

class GraphicView: UIView {
    private let scrollView = UIScrollView.create()
    private let containerView = UIView.create()
    
    private let gridLayer = CAShapeLayer()
    private let shapeLayer = CAShapeLayer()
    
    var fitGraphic: Bool = true {
        didSet {
            updateFitLayout()
        }
    }
    
    var viewModel: GraphicViewModel? {
        didSet {
            redraw()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        addSubview(scrollView)
        scrollView.constraintsToSuperview(withInsets: .zero).activate()
        scrollView.addSubview(containerView)
        containerView.layer.addSublayer(gridLayer)
        containerView.layer.addSublayer(shapeLayer)
        updateFitLayout()
    }
    
    private func updateFitLayout() {
        containerView.removeConstraints(containerView.constraints)
        if fitGraphic {
            containerView.constraintsToSuperview(withInsets: .zero).activate()
            [containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
             containerView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)].activate()
        } else {
            [containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
             containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)].activate()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        redraw()
    }
    
    private func redraw() {
        createGrid()
    }
    
    private var graphInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
    
    private func createGrid() {
        /*
        guard let viewModel = self.viewModel else {
            return
        }
        let leftInset = graphInsets.left
        let width = containerView.bounds.width - graphInsets.left - graphInsets.right
        let height = self.containerView.frame.height - graphInsets.bottom - graphInsets.top
        var maxValue = viewModel.maximumValue
        if maxValue < 10 {
            maxValue = 10
        }
        let makeHorisontalLine: ((Float) -> CGFloat) = { value in
            height - height * CGFloat(value / maxValue)
        }
        
        let path = UIBezierPath()
        
        for i in 0 ... Int(maxValue / 10) + 1 {
            let lineHeigh = makeHorisontalLine(Float(i * 10))
            path.move(to: CGPoint(x: leftInset, y: lineHeigh))
            path.addLine(to: CGPoint(x: leftInset + width, y: lineHeigh))
        }
        gridLayer.lineWidth = 1
        gridLayer.path = path.cgPath
        gridLayer.strokeColor = UIColor.systemGray.withAlphaComponent(0.5).cgColor
        gridLayer.fillColor = UIColor.clear.cgColor
        maxValueLabel.text = "\(Int(viewModel.maximumValue)) мл"
        maxDateLabel.text = ""//Максимум " + DateFormatter.timeAndDateIfToday(viewModel.maximumDate)
         */
    }
}
