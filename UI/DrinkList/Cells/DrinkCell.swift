//
//  AlcoCell.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 23.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit
import Model
import Costelique

struct DrinkCellViewModel {
    let drink: Drink
    let showDate: Bool
    let progress: () -> Float
}

final class DrinkCell: UITableViewCell {

    @IBOutlet private weak var lblPercents: UILabel!
    @IBOutlet private weak var lblVolume: UILabel!
    @IBOutlet private weak var lblTime: UILabel!
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var lblZakus: UILabel!
    @IBOutlet private weak var progressView: UIProgressView!
    @IBOutlet private weak var progressContainer: UIView!
    @IBOutlet private weak var timeTopInset: NSLayoutConstraint!
    
    var doubleTapHandler: ((DrinkCell) -> Void)?
    var progressGetter: (() -> Float)?
    
    private(set) var representedObject: Drink?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(Self.doubleTapAction))
        recognizer.numberOfTapsRequired = 2
        contentView.addGestureRecognizer(recognizer)
    }
    
    func configure(model: DrinkCellViewModel) {
        self.representedObject = model.drink
        self.lblDate.isHidden = !model.showDate
        self.progressGetter = model.progress
        updateCell()
    }
    
    func updateProgress() {
        self.progress = progressGetter?() ?? 1
    }
    
    private var progress: Float = 0 {
        didSet {
            if progress == 1 {
                progressContainer.isHidden = true
            } else {
                progressContainer.isHidden = false
                progressView.progress = progress
            }
            updateTopInset()
        }
    }
    
    @objc
    private func doubleTapAction() {
        doubleTapHandler?(self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        representedObject = nil
        updateCell()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        updateCell()
    }

    private func updateCell() {
        guard let drink = representedObject else {
            lblTime.text = "error"
            return
        }
        
        self.lblPercents.attributedText = {
            let percentRounded = (drink.percents * 10).rounded() / 10
            let percentString = String(format: "%g", percentRounded)
            let attributed = TextStyler(.font(size: 18, weight: .regular)).mutable(percentString)
            attributed.append(TextStyler(.font(size: 16, weight: .light)).attributed(" %"))
            return attributed
        }()
        
        self.lblVolume.attributedText = {
            let attributed = TextStyler(.font(size: 18, weight: .regular)).mutable("\(Int(drink.milliliters.rounded()))")
            attributed.append(TextStyler(.font(size: 16, weight: .light)).attributed(" мл"))
            return attributed
        }()
        
        lblZakus.text = drink.zakusDescription
        lblZakus.isHidden = drink.zakus == 0
        
        lblTime.text = DateFormatter.timeFormatter.string(from: drink.date)
        lblDate.text = DateFormatter.dayMounthShort.string(from: drink.date)
        updateTopInset()
    }
    
    private func updateTopInset() {
        var inset: CGFloat = 8
        if !progressContainer.isHidden || lblDate.isHidden {
            inset += 7
        }

        timeTopInset.constant = inset
    }
}

extension Drink {
    var zakusDescription: String {
        Drink.zakusDescription(self.zakus)
    }
    
    static func zakusDescription(_ value: Float) -> String {
        switch value {
        case 0.0 ..< 0.1: return "Без закуски"
        case 0.1 ..< 0.6: return "Легкая закуска"
        case 0.6 ..< 1.1: return "Средняя закуска"
        case 1.1 ..< 1.6: return "Плотная закуска"
        case 1.6 ... 2.0: return "Застолье"
        default: return ""
        }
    }
}
