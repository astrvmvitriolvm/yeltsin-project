//
//  EditViewController.swift
//  Alcohelper
//
//  Created by kogami on 21.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Model
import Costelique

final class EditDrinkController: BaseViewController {
    
    @IBOutlet private weak var fldPercents: UITextField!
    @IBOutlet private weak var fldVolume: UITextField!
    @IBOutlet private weak var startTime: UITextField!
    @IBOutlet private weak var endTime: UITextField!
    @IBOutlet private weak var datePicker: UIDatePicker!
    @IBOutlet private weak var sldZakus: UISlider!
    
    static func create() -> EditDrinkController {
        self.instantinateFromNib()
    }
    
    var viewModel: EditDrinkViewModel!
    var initial: Drink?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(viewModel != nil)
        initial = viewModel.drink
        fldPercents.delegate = self
        fldPercents.keyboardType = .numbersAndPunctuation
        fldVolume.delegate = self
        fldVolume.keyboardType = .numbersAndPunctuation
        startTime.delegate = self
        startTime.keyboardType = .numbersAndPunctuation
        endTime.delegate = self
        endTime.keyboardType = .numbersAndPunctuation
        sldZakus.maximumValue = Drink.zakusMaxValue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateView()
    }
    
    private func updateView() {
        guard isViewLoaded else {
            return
        }
        fldPercents.text = String(format: "%g", viewModel.drink.percents.rounded(digits: 1))
        fldVolume.text = String(format: "%g", viewModel.drink.milliliters.rounded(digits: 1))
        startTime.text = DateFormatter.timeFormatter.string(from: viewModel.drink.date)
        endTime.text = DateFormatter.timeFormatter.string(from: viewModel.drink.endDate ?? viewModel.drink.date)
        datePicker.date = viewModel.drink.date
        sldZakus.value = viewModel.drink.zakus
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        save()
    }
    
    private func save() {
        guard initial != nil else {
            return
        }
        guard viewModel?.drink != nil else {
            return
        }
        let percentText = fldPercents.text?.replacingOccurrences(of: ",", with: ".") ?? ""
        let volumeText = fldVolume.text?.replacingOccurrences(of: ",", with: ".") ?? ""
        guard var percent = Float(percentText), percent > 0, var volume = Float(volumeText), volume > 0 else {
            return
        }
        percent = min(percent, 100)
        volume = min(volume, 5000)
        let day = datePicker.date.round(to: .day)
        guard let start = startTime.getTime() else {
            return
        }
        let startDate = day.addingTimeInterval(.hour * Double(start.hours) + .minute * Double(start.minutes))
        var endDate: Date?
        if let end = endTime.getTime() {
            let endCandidate = day.addingTimeInterval(.hour * Double(end.hours) + .minute * Double(end.minutes))
            if endCandidate > startDate {
                endDate = endCandidate
            }
        }
        let zakus = sldZakus.value
        let newDrink = Drink(milliliters: volume,
                             percents: percent,
                             date: startDate,
                             zakus: zakus,
                             endDate: endDate)
        viewModel?.save(drink: newDrink)
    }
    
    @IBAction func cancelHandler(_ sender: Any) {
        guard let initial = initial else {
            dismiss(animated: true, completion: nil)
            return
        }
        self.initial = nil
        viewModel.save(drink: initial)
        updateView()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sliderEditEnd(_ sender: Any) {
        let step: Float = 0.5
        let roundedValue = round(sldZakus.value / step) * step
        sldZakus.value = roundedValue
    }
}

extension EditDrinkController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        save()
        updateView()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        let newText = (text as NSString).replacingCharacters(in: range, with: string) as String

        switch textField {
        case fldVolume, fldPercents:
            let charset = CharacterSet(charactersIn: "1234567890,.").inverted
            if string.rangeOfCharacter(from: charset) != nil {
                return false
            }
            
        case startTime, endTime:
            let charset = CharacterSet(charactersIn: "1234567890:").inverted
            guard string.rangeOfCharacter(from: charset) == nil, newText.count < 6 else {
                return false
            }
            if string == ":", text.range(of: ":") != nil {
                return false
            }
            if newText.count > 2, newText.range(of: ":") == nil {
                var newText = newText
                newText.insert(":", at: String.Index(utf16Offset: 2, in: string))
                textField.text = newText
                return false
            }
            
        default:
            break
        }
        
        return true
    }
}

private extension UITextField {
    func getTime() -> (hours: Int, minutes: Int)? {
        if let elements = text?.split(separator: ":"),
           elements.count == 2,
           let hours = Int(elements.first ?? ""), hours < 25,
           let minutes = Int(elements.last ?? ""), minutes < 61 {
            return (hours, minutes)
        }
        return nil
    }
}
