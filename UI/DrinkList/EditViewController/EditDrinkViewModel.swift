//
//  EditDrinkViewModel.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 31.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Model

final class EditDrinkViewModel {
    private let drinkManager: DrinkManager
    private(set) var drink: Drink
    
    init(drink: Drink, drinkManager: DrinkManager) {
        self.drinkManager = drinkManager
        self.drink = drink
    }
    
    func save(drink newDrink: Drink) {
        guard !newDrink.isSame(drink) else {
            return
        }
        drinkManager.replace(drink: drink, with: newDrink)
        drink = newDrink
    }
}
