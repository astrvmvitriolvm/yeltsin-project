//
//  AlcoListViewModel.swift
//  Alcohelper
//
//  Created by kogami on 17.02.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Costelique
import Model
import Base

final class DrinkListViewModel {
    private let defaultsManager: DefaultsManaging
    private let alcoManager: DrinkManager
    private var sections: [DrinkListSection] = []
    private var sectionsToShow = 0
    private var currentPage = 0
    
    var updateHandler: (()->Void)?
    
    var warningLevel: DrinkManager.WarningLevel {
        alcoManager.warningLevel
    }

    init(defaultsManager: DefaultsManaging, drinkManager: DrinkManager) {
        self.defaultsManager = defaultsManager
        self.alcoManager = drinkManager
        drinkManager.queues.addObserver(self) { strongSelf, queues in
            let sections = queues.reversed().map { DrinkListSection(queue: $0) }
            DispatchQueue.main.async {
                strongSelf.updateModel(sections)
            }
        }
        updateDefaults()
        loadNextPage()
    }
    
    private func updateModel(_ sections: [DrinkListSection]) {
        self.sections = sections
        if sectionsToShow == 0 {
            loadNextPage()
        }
        updateHandler?()
    }

    //MARK: - Public
    var sectionsCount: Int {
        if defaultsManager.hideAllExceptLast {
            return alcoManager.currentQueue == nil ? 0 : min(1, sections.count)
        }
        return min(sectionsToShow, sections.count)
    }
    
    func cellsInSection(_ int: Int) -> Int {
        sections[int].count
    }
    
    func headerForSection(_ int: Int) -> String {
        sections[int].title
    }
    
    func element(at indexPath: IndexPath) -> DrinkListElement {
        sections[indexPath.section].cells[indexPath.item]
    }
    
    func add(drink: Drink) {
        alcoManager.add(drink: drink)
    }
    
    func remove(drink: Drink) {
        alcoManager.remove(drink: drink)
    }
    
    func updateDate(drink: Drink, newDate: Date) {
        let newDrink = Drink(milliliters: drink.milliliters, percents: drink.percents, date: newDate, zakus: drink.zakus)
        alcoManager.replace(drink: drink, with: newDrink)
    }
    
    func updateDefaults() {
        alcoManager.updateUserCharacteristics(weight: Float(defaultsManager.weight), gender: defaultsManager.gender)
    }
    
    func loadNextPage() {
        let sectionsShown = sectionsToShow
        var drinksAdded = 0
        guard sectionsShown < sections.count else {
            return
        }
        for i in sectionsShown ..< sections.count {
            sectionsToShow = min(sectionsToShow + 1, sections.count)
            drinksAdded += sections[i].count
            if drinksAdded > 1000 {
                break
            }
        }
        if sectionsShown != sectionsToShow {
            self.updateHandler?()
        }
    }

}
