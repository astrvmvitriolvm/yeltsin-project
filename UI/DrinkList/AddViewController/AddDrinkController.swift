//
//  AddViewController.swift
//  Alcohelper
//
//  Created by kogami on 14.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit
import Model
import Base

final class AddDrinkController: BaseViewController {
    
    static func create(context: DrinkContext) -> AddDrinkController {
        let vc = self.instantinateFromNib()
        vc.context = context
        return vc
    }
    
    @IBOutlet private weak var fldPercents: UITextField!
    @IBOutlet private weak var fldVolume: UITextField!
    @IBOutlet private weak var addBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var sldZakus: UISlider!
    @IBOutlet private weak var lblZakus: UILabel!
    @IBOutlet private weak var btnAdd: UIButton!
    
    private static var percent: Float = 0
    private static var volume: Float = 0
    private var context: DrinkContext!
    
    var viewModel: AddDrinkViewModel!
    var completion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(viewModel != nil)
        title = "Добавьте алкоголь"
        fldPercents.keyboardType = .numbersAndPunctuation
        fldPercents.delegate = self
        fldPercents.autocorrectionType = .no
        fldVolume.keyboardType = .numbersAndPunctuation
        fldVolume.delegate = self
        fldVolume.autocorrectionType = .no
        sldZakus.maximumValue = Drink.zakusMaxValue
        
        if Self.percent > 0, Self.volume > 0 {
            fldPercents.text = String(format: "%g", Self.percent.rounded(digits: 1))
            fldVolume.text = String(format: "%g", Self.volume.rounded(digits: 1))
        } else {
            fldPercents.text = ""
            fldVolume.text = ""
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fldPercents.becomeFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func addAction(_ sender: Any) {
        let percentText = fldPercents.text?.replacingOccurrences(of: ",", with: ".") ?? ""
        let volumeText = fldVolume.text?.replacingOccurrences(of: ",", with: ".") ?? ""
        guard var percent = Float(percentText)?.rounded(digits: 1), percent > 0,
              var volume = Float(volumeText)?.rounded(digits: 1), volume > 0 else {
            return
        }
        view.endEditing(true)
        percent = min(percent, 100)
        volume = min(volume, 6000)
        let zakus = sldZakus.value
        
        if percent == 26, volume == 93 {
            context.startDemo()
            return
        }
        let date = Date().round(to: .second)
        let drink = Drink(milliliters: volume, percents: percent, date: date, zakus: zakus)
        viewModel.add(drink: drink)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sliderZakusChange(_ sender: UISlider) {
        let step: Float = 0.5
        let roundedValue = round(sender.value / step) * step
        sender.value = roundedValue
        updateZakusSlider()
    }
    
    private func updateZakusSlider() {
        let value = Float(sldZakus.value)
        lblZakus.text = Drink.zakusDescription(value)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.btnAdd.layer.cornerRadius = btnAdd.bounds.height/2
    }
}

extension AddDrinkController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case fldPercents:
            let percentText = fldPercents.text?.replacingOccurrences(of: ",", with: ".") ?? ""
            if let percent = Float(percentText), percent > 0 {
                Self.percent = percent
            }
            
        case fldVolume:
            let volumeText = fldVolume.text?.replacingOccurrences(of: ",", with: ".") ?? ""
            if let volume = Float(volumeText), volume > 0 {
                Self.volume = volume
            }
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let charset = CharacterSet(charactersIn: "1234567890,.").inverted
        if string.rangeOfCharacter(from: charset) != nil {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case fldPercents:
            fldVolume.becomeFirstResponder()
            
        case fldVolume:
            fldVolume.resignFirstResponder()
            addAction(textField)
            
        default:
            break
        }
        return true
    }
}

extension AddDrinkController: KeyboardWatcher {
    func keyboardWillShow(frame: CGRect, duration: Double) {
        addBottomConstraint.constant = frame.height - (self.safeArea.bottom)
        UIView.animate(withDuration: duration) {
            self.view.superview?.superview?.layoutIfNeeded()
        }
    }

    func keyboardWillHide(frame: CGRect, duration: Double) {
        addBottomConstraint.constant = 0
        UIView.animate(withDuration: duration) {
            self.view.superview?.superview?.layoutIfNeeded()
        }
    }
}
