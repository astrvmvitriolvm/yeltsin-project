//
//  AddViewModel.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 31.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Model

final class AddDrinkViewModel {
    private let drinkManager: DrinkManager
    
    init(drinkManager: DrinkManager) {
        self.drinkManager = drinkManager
    }
    
    func add(drink: Drink) {
        drinkManager.add(drink: drink)
    }
}
