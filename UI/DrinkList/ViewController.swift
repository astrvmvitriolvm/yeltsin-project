//
//  ViewController.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 23.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import UIKit

class AlcoList: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var lblEtanol: UILabel!
    @IBOutlet private weak var lblTimeLeft: UILabel!

    private let alcoManager = AlcoManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateLabels()
        self.updateTable()
    }

    private func updateLabels() {
        let ml = alcoManager.millilitersInBlood
        let timeLeft = Int(alcoManager.detoxicationTime.timeIntervalSinceNow) / 60
        
        self.lblEtanol.text = "Этанола в крови: \(ml) мг"
        self.lblTimeLeft.text = "Время до выведения: \(timeLeft) м"
    }

    private func updateTable() {
        self.tableView.reloadData()
    }
}

