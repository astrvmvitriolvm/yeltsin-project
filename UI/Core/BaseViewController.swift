//
//  BaseViewController.swift
//  BongaTalk
//
//  Created by Oleg Matveev on 10.09.2018.
//  Copyright © 2018 AITI MEGASTAR. All rights reserved.
//
//swiftlint:disable weak_delegate

import UIKit

public protocol KeyboardWatcher: BaseViewController {
    func keyboardWillShow(frame: CGRect, duration: Double)
    func keyboardWillHide(frame: CGRect, duration: Double)
}

open class BaseViewController: UIViewController {
    private let touchDelegate = TouchDelegate()
    
    public let safeArea: UIEdgeInsets = {
        UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.safeAreaInsets ?? .zero
    }()
    
    public private(set) var isVisible: Bool = false
    //MARK: - Lifecycle

    open override func viewDidLoad() {
        super.viewDidLoad()
        self.addTapRecognizer()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isVisible = true
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeydoard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeydoard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isVisible = false
    }

    //MARK: - Keyboard

    @objc private func adjustForKeydoard(notification: Notification) {
        let info = notification.userInfo
        if let keyboardFrame = (info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let duration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let willHide = notification.name == UIResponder.keyboardWillHideNotification
            self.adjustForKeydoard(with: keyboardFrame, duration: duration, willHide: willHide)
        }
    }
    
    private func adjustForKeydoard(with frame: CGRect, duration: Double, willHide: Bool) {
        if let watcher = self as? KeyboardWatcher {
            if willHide {
                watcher.keyboardWillHide(frame: frame, duration: duration)
            } else {
                watcher.keyboardWillShow(frame: frame, duration: duration)
            }
        }
        guard let scrollView = view.subviews.first as? UIScrollView else {
            return
        }

        if willHide {
            scrollView.contentInset = safeArea
        } else {
            var insets = safeArea
            insets.bottom += frame.height
            scrollView.contentInset = insets
        }

        UIView.animate(withDuration: duration, animations: {
            self.view.layoutIfNeeded()
        })

        if !willHide, let responder = self.view.firstResponder() {
            let responderFrame = responder.convert(responder.bounds, to: nil)
            let delta = responderFrame.origin.y + responderFrame.size.height - frame.origin.y

            if delta > 0 {
                let newOffset = CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y + delta)
                UIView.animate(withDuration: duration, animations: {
                    scrollView.contentOffset = newOffset
                })
            }
        }
    }
    
    //MARK: - Taps
    
    private func addTapRecognizer() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        recognizer.cancelsTouchesInView = false
        recognizer.delegate = self.touchDelegate
        view.addGestureRecognizer(recognizer)
    }
    
    @objc private func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(false)
    }
}

fileprivate extension BaseViewController {
    class TouchDelegate: NSObject, UIGestureRecognizerDelegate {
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            !(touch.view is UIButton)
        }
    }
}
