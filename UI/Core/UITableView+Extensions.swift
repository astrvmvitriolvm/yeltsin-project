//
//  UITableView+Extensions.swift
//  Base
//
//  Created by Данис Тазетдинов on 30.07.2018.
//  Copyright © 2018 Danis Tazetdinov. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeue<T: UITableViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.classIdentifier, for: indexPath) as? T else {
            return T(style: .default, reuseIdentifier: T.classIdentifier)
        }
//        cell.frame = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height)
//        cell.setNeedsLayout()
//        cell.layoutIfNeeded()
        return cell
    }

    func registerClass<T: UITableViewCell>(forCell type: T.Type) {
        register(T.self, forCellReuseIdentifier: T.classIdentifier)
    }

    func registerClass<T: UITableViewHeaderFooterView>(forHeaderFooter type: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.classIdentifier)
    }

    func registerNib<T: UITableViewCell>(forCell type: T.Type) {
        register(UINib(nibName: T.classIdentifier, bundle: Bundle(for: T.self)), forCellReuseIdentifier: T.classIdentifier)
    }

    func registerNib<T: UITableViewHeaderFooterView>(forHeaderFooter type: T.Type) {
        register(UINib(nibName: T.classIdentifier, bundle: Bundle(for: T.self)), forHeaderFooterViewReuseIdentifier: T.classIdentifier)
    }
    
    /*
    func setTableHeaderColor(_ color: UIColor) {
        let headerView = UIView(frame: .zero)
        headerView.clipsToBounds = false
        let coloredView = UIView(frame: UIScreen.main.bounds)
        coloredView.backgroundColor = color
        headerView.addSubview(coloredView)
        coloredView.frame.origin.y = -coloredView.frame.size.height
        tableHeaderView = headerView
    }
    
    func setTableFooterColor(_ color: UIColor) {
        let footerView = UIView(frame: .zero)
        footerView.clipsToBounds = false
        let coloredView = UIView(frame: UIScreen.main.bounds)
        coloredView.backgroundColor = color
        footerView.addSubview(coloredView)
        tableFooterView = footerView
    }*/
}
