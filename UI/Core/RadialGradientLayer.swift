//
//  RadialGradient.swift
//  Alcohelper
//
//  Created by kogami on 20.02.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import UIKit

public final class RadialGradientLayer: CALayer {
    
    public var colors: [UIColor] = [UIColor.black, UIColor.lightGray] {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var center: CGPoint {
        CGPoint(x: bounds.width/2, y: bounds.height/2)
    }

    var radius: CGFloat {
        (bounds.width + bounds.height)/2
    }

    var cgColors: [CGColor] {
        colors.map { $0.cgColor }
    }

    public override init() {
        super.init()
        needsDisplayOnBoundsChange = true
    }

    required init(coder aDecoder: NSCoder) {
        super.init()
    }

    public override func draw(in ctx: CGContext) {
        ctx.saveGState()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations: [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
            return
        }
        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
    }

}
