//
//  UICollectionView+Extensions.swift
//  Base
//
//  Created by Данис Тазетдинов on 30.07.2018.
//  Copyright © 2018 Danis Tazetdinov. All rights reserved.
//

import UIKit

extension UICollectionView {
    public func dequeue<T: UICollectionViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.classIdentifier, for: indexPath) as? T else {
            fatalError("cannot construct \(T.self)")
        }
        return cell
    }

    public func registerClass<T: UICollectionViewCell>(forCell type: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.classIdentifier)
    }

    public func registerNib<T: UICollectionViewCell>(forCell type: T.Type) {
        register(UINib(nibName: T.classIdentifier, bundle: Bundle(for: T.self)), forCellWithReuseIdentifier: T.classIdentifier)
    }
}
