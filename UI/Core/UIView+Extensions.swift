//
//  UIView+Extensions.swift
//  UI
//
//  Created by Матвеев Олег on 18.05.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    static func create() -> Self {
        let view = Self.init(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}
