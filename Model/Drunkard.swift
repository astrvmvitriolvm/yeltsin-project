//
//  Drunkard.swift
//  Model
//
//  Created by Матвеев Олег on 22.04.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public class Drunkard: Codable {
    public var name: String
    public var drinks: [Drink]
    public var weight: Double
    public var gender: Gender
    
    public init(name: String, drinks: [Drink], weight: Double, gender: Gender) {
        self.name = name
        self.drinks = drinks
        self.weight = weight
        self.gender = gender
    }
}
