//
//  TimePeriod.swift
//  Model
//
//  Created by Матвеев Олег on 02.11.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public enum TimePeriod: Int, Codable, CaseIterable {
    case day
    case week
    case month
    case year
    
    var name: String {
        switch self {
        case .day: return "День"
        case .week: return "Неделя"
        case .month: return "Месяц"
        case .year: return "Год"
        }
    }
}
