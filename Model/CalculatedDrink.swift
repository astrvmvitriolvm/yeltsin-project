//
//  CalculatedDrink.swift
//  YeltsinProject
//
//  Created by Матвеев Олег on 31.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

internal final class CalculatedDrink {
    let drink: Drink
    let detoxStart: Date
    let detoxEnd: Date
    
    lazy var absorbtionEnd: Date = {
        let absorbtionTime: Float = 30 // in minutes
        let percentKoeff = 10 * (1 - min(1, max(0, abs(15 - Float(drink.percents))/15)))
        let progress = Double(absorbtionTime * (1 + drink.zakus * 1.5) - percentKoeff)
        if let endDate = drink.endDate {
            return endDate.addingTimeInterval(progress * 60)
        }
        return drink.date.addingTimeInterval(progress * 60)
    }()
    
    init(drink: Drink, detoxStart: Date, detoxEnd: Date) {
        self.drink = drink
        self.detoxStart = detoxStart
        self.detoxEnd = detoxEnd
    }
}
