//
//  Drink.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 23.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import Foundation

public class Drink: Codable {
    public static let zakusMaxValue: Float = 2
    
    public let milliliters: Float
    public let percents: Float
    public let date: Date
    public let zakus: Float //0 - 2
    public var endDate: Date?
    
    public init(milliliters: Float, percents: Float, date: Date, zakus: Float, endDate: Date? = nil) {
        self.milliliters = milliliters
        self.percents = percents
        self.date = date
        self.zakus = zakus
        self.endDate = endDate
    }
    
    public func isSame(_ drink: Drink) -> Bool {
        milliliters == drink.milliliters
            && percents == drink.percents
            && date == drink.date
            && zakus == drink.zakus
            && endDate == drink.endDate
    }
}

extension Drink {
    var ethanol: Float {
        milliliters * percents / 100
    }
}

extension Drink: Equatable {
    public static func == (lhs: Drink, rhs: Drink) -> Bool {
        lhs === rhs
    }
}
