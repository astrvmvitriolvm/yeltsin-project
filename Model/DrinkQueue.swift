//
//  File.swift
//  Alcohelper
//
//  Created by kogami on 24.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation
import Costelique
import CoreGraphics

public protocol DrinkQueuing {
    var count: Int { get }
    var first: Drink { get }
    var last: Drink { get }
    var milliliters: Float { get }
    var isCurrent: Bool { get }
    func millilitersInBlood(at date: Date) -> Float
    func detoxicationProgress(of drink: Drink, at date: Date) -> Float
    func getAllDrinks() -> [Drink]
    func getTimeline() -> GraphData
    func expectedMaximum() -> DatedValue<Float>
}

internal final class DrinkQueue {
    private enum Constants {
        static let maxControlPoints: Int = 150
    }
    
    private let calculatedDrinks: [CalculatedDrink]
    var detoxDate: Date {
        calculatedDrinks.last!.detoxEnd
    }
    
    var startDate: Date {
        calculatedDrinks.first!.drink.date
    }
    
    public lazy var milliliters: Float = {
        calculatedDrinks.reduce(0, { $0 + $1.drink.ethanol })
    }()

    private lazy var controlPoints: [DatedValue<Float>] = {
        AKTimer.start()
        let cds = calculatedDrinks//self.getSmoothPoints()
        guard cds.count < Constants.maxControlPoints / 2 else {
            let points = getPointsFast(cds)
            AKTimer.log("getPointsFast()")
            return points
        }
        let points = getPointsAccurate(cds)
        AKTimer.log("getPointsAccurate()")
        return points
    }()
    
    init?(_ drinks: [CalculatedDrink]) {
        guard drinks.count > 0 else {
            return nil
        }
        self.calculatedDrinks = drinks
    }
    
    func isInclude(_ date: Date) -> Bool {
        date >= startDate && date <= detoxDate
    }
    
    func millilitersInBlood(at date: Date, using: [CalculatedDrink]?) -> Float {
        var milliliters: Float = 0
        let cds = using ?? calculatedDrinks
        for cd in cds where cd.drink.date < date && date < cd.detoxEnd {
            let absorbtion = progressOfAbsorbtion(of: cd, at: date)
            let detoxication = detoxicationProgress(of: cd, at: date)
            milliliters += max(0, cd.drink.ethanol * (absorbtion - detoxication))
        }
        
        return milliliters
    }
    
    private func detoxicationProgress(of cd: CalculatedDrink, at date: Date) -> Float {
        let progress = Float(date.timeIntervalSince(cd.detoxStart) / cd.detoxEnd.timeIntervalSince(cd.detoxStart))
        return min(1, max(0, Float(progress)))
    }
    
    private func progressOfAbsorbtion(of cd: CalculatedDrink, at date: Date) -> Float {
        guard date < cd.absorbtionEnd else { return 1 }
        guard date > cd.drink.date else { return 0 }
        let linearProgress = date.timeIntervalSince(cd.drink.date) / cd.absorbtionEnd.timeIntervalSince(cd.drink.date)
        let function = TimingFunction(controlPoint1: CGPoint(x: 0.4, y: 0.2), controlPoint2: CGPoint(x: 0.6, y: 1), duration: 10)
        return Float(function.progress(at: CGFloat(linearProgress)))
    }
    
    private func progressOfAbsorbtionLinear(of cd: CalculatedDrink, at date: Date) -> Float {
        let progress = date.timeIntervalSince(cd.drink.date) / cd.absorbtionEnd.timeIntervalSince(cd.drink.date)
        return min(1, max(0, Float(progress)))
    }
    
    private func getSmoothPoints() -> [CalculatedDrink] {  // for testing
        var cds = calculatedDrinks
        for i in 0 ..< cds.count {
            let cd = cds[i]
            if let endDate = cd.drink.endDate {
                cds.remove(at: i)
                let count = 20
                let oneStep = endDate.timeIntervalSince(cd.drink.date) / Double(count)
                let detoxStep = cd.detoxEnd.timeIntervalSince(cd.detoxStart) / Double(count)
                for j in 0 ..< count {
                    let drink = Drink(milliliters: cd.drink.milliliters / Float(count), percents: cd.drink.percents, date: cd.drink.date.addingTimeInterval(oneStep * Double(j)), zakus: cd.drink.zakus)
                    let new = CalculatedDrink(drink: drink,
                                             detoxStart: cd.detoxStart.addingTimeInterval(detoxStep * Double(j)),
                                             detoxEnd: cd.detoxStart.addingTimeInterval(detoxStep * Double(j + 1)))
                    cds.insert(new, at: i + j)
                }
            }
        }
        return cds
    }
    
    private func getPointsAccurate(_ cds: [CalculatedDrink]) -> [DatedValue<Float>] {
        let start = startDate
        let end = cds.last!.absorbtionEnd
        let stepsCount = Constants.maxControlPoints - 2
        let step = end.timeIntervalSince(start) / Double(stepsCount)
        var cp: [Date] = []
        for i in 0 ... stepsCount {
            cp.append(start.addingTimeInterval(step * Double(i)))
        }
        cp.append(detoxDate)
        let dates = cp.map({ DatedValue(date: $0, value: millilitersInBlood(at: $0, using: cds))  })
        return dates
    }
    
    private func getPointsFast(_ cds: [CalculatedDrink]) -> [DatedValue<Float>] {
        let groupsCount = 100
        let start = startDate
        let step = cds.last!.drink.date.timeIntervalSince(start) / Double(groupsCount - 1)
        var simplified: [CalculatedDrink] = []
        var drinkIndex = 0
        for i in 0 ..< groupsCount {
            let attractionPoint = start.addingTimeInterval(step * Double(i))
            var group: [CalculatedDrink] = []
            var ethanol: Float = 0
            
            while drinkIndex < cds.count {
                let cd = cds[drinkIndex]
                if abs(cd.drink.date.timeIntervalSince(attractionPoint)) > step / 2 {
                    break
                }
                ethanol += cd.drink.ethanol
                group.append(cd)
                drinkIndex += 1
            }
            if group.count == 1 {
                simplified.append(group.first!)
            } else if !group.isEmpty {
                let drink = Drink(milliliters: ethanol, percents: 100, date: group.first!.drink.date, zakus: 0)
                let cd = CalculatedDrink(drink: drink, detoxStart: group.first!.detoxStart, detoxEnd: group.last!.detoxEnd)
                simplified.append(cd)
            }
        }
        
        let end = simplified.last!.absorbtionEnd
        let stepsCount = Constants.maxControlPoints - 2
        let cpStep = end.timeIntervalSince(start) / Double(stepsCount)
        var cp: [Date] = []
        for i in 0 ... stepsCount {
            cp.append(start.addingTimeInterval(cpStep * Double(i)))
        }
        cp.append(detoxDate)
        let dates = cp.map({ DatedValue(date: $0, value: millilitersInBlood(at: $0, using: simplified))  })
        return dates
    }
}

extension DrinkQueue: DrinkQueuing {
    public var count: Int {
        calculatedDrinks.count
    }
    
    public var first: Drink {
        calculatedDrinks.first!.drink
    }
    
    public var last: Drink {
        calculatedDrinks.last!.drink
    }
    
    public func getAllDrinks() -> [Drink] {
        calculatedDrinks.map { $0.drink }
    }
    
    public func detoxicationProgress(of drink: Drink, at date: Date) -> Float {
        guard let cd = calculatedDrinks.first(where: { $0.drink == drink }) else {
            print("WARNING: detoxicationProgress(of drink: Drink, at date: Date) -> Float, no drink in queue")
            return drink.date < date ? 0 : 1
        }
        return detoxicationProgress(of: cd, at: date)
    }
    
    public func getTimeline() -> GraphData {
        GraphData(points: controlPoints)
    }
    
    public func expectedMaximum() -> DatedValue<Float> {
//        graphData.points.reduce(0, { max($0, $1.value) })
        controlPoints.max(by: { $0.value < $1.value })!
    }
    
    public func millilitersInBlood(at date: Date) -> Float {
        self.millilitersInBlood(at: date, using: nil)
    }
    
    var isCurrent: Bool {
        calculatedDrinks.first!.drink.date < Date() && calculatedDrinks.last!.detoxEnd > Date()
    }
}
