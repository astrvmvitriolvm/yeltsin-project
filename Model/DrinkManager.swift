//
//  EtanolManager.swift
//  Alcohelper
//
//  Created by Oleg Matveev on 23.07.2020.
//  Copyright © 2020 Oleg Matveev. All rights reserved.
//

import Foundation
import Costelique

public protocol DrinkManagerDelegate: AnyObject {
    func drinkManager(drinksChanged: [Drink])
}

public final class DrinkManager {
    
    private var queued: [DrinkQueue] = []
    
    private let propertysQueue = DispatchQueue(label: "DrinkManager.queue", qos: .userInitiated, attributes: .concurrent)
    private let sortingQueue: OperationQueue = {
        let q = OperationQueue()
        q.maxConcurrentOperationCount = 1
        q.qualityOfService = .userInitiated
        return q
    }()
    
    
    public private(set) var weight: Float = 80
    public private(set) var gender: Gender = .male
    public private(set) var drinks: [Drink]
    public private(set) var queues: Observable<[DrinkQueuing]> = .init([])
    public private(set) var isLoading: Observable<Bool> = .init(false)
    
    public weak var delegate: DrinkManagerDelegate?
    
    public init(with drinkSource: @escaping () -> [Drink]) {
        self.drinks = []
        DispatchQueue.global(qos: .userInteractive).async {
            self.propertysQueue.sync(flags: .barrier) {
                self.drinks = drinkSource()
            }
            self.sortAndGroup()
        }
    }
    
    private func sortAndGroup() {
        let drinks = propertysQueue.sync { self.drinks }
        sortingQueue.cancelAllOperations()
        sortingQueue.addOperation { [weak self] in
            guard let self = self else {
                return
            }
            
            AKTimer.start()
            let sorted = drinks.sorted(by: { $0.date < $1.date })
            self.propertysQueue.sync(flags: .barrier) {
                self.drinks = sorted
            }
            let groups = self.getGroups(fromSorted: sorted)
            self.propertysQueue.sync(flags: .barrier) {
                self.queued = groups
            }
            self.isLoading.wrappedValue = false
            self.queues.wrappedValue = self.propertysQueue.sync { self.queued }
        }
    }
    
    private func getGroups(fromSorted sorted: [Drink]) -> [DrinkQueue] {
        guard let first = sorted.first else {
            return []
        }
        let detoxSpeed = self.detoxSpeed()
        var groups: [DrinkQueue?] = []
        var currentQueue: [CalculatedDrink] = []
        var prevDate = first.date
        for drink in sorted {
            if drink.date > prevDate {
                groups.append(DrinkQueue(currentQueue))
                let detoxStart = drink.date
                let timeToDetoxify = TimeInterval(exactly: drink.ethanol / detoxSpeed * 3600) ?? 0
                let detoxEnd = drink.date.advanced(by: timeToDetoxify)
                currentQueue = [CalculatedDrink(drink: drink, detoxStart: detoxStart, detoxEnd: detoxEnd)]
                prevDate = detoxEnd
            } else {
                let detoxStart = prevDate
                let timeToDetoxify = TimeInterval(exactly: drink.ethanol / detoxSpeed * 3600) ?? 0
                let detoxEnd = prevDate.advanced(by: timeToDetoxify)
                currentQueue.append(CalculatedDrink(drink: drink, detoxStart: detoxStart, detoxEnd: detoxEnd))
                prevDate = detoxEnd
            }
        }
        groups.append(DrinkQueue(currentQueue))
        return groups.compactMap { $0 }
    }
    
    private func tricksForLargeNumbers() {
        var minNumber = Int.max
        var maxNumber = 0
        let timer = AKTimer()
        for _ in 0 ..< 100 {
            timer.start()
            let degree: Double = sqrt(Double(drinks.count)) / 50
            let intervals: [TimeInterval] = (0 ... 1000).map { _ in drinks.randomElement()!.date.timeIntervalSinceNow }
            let middle: TimeInterval = intervals.reduce(0, { $0 + $1 }) / Double(intervals.count)
            let dispersion: TimeInterval = intervals.reduce(0, { $0 + pow(abs($1 - middle), degree) }) / Double(intervals.count)
            let devation = pow(dispersion, 1 / degree)
            let reference = Date().addingTimeInterval(middle + devation)
            timer.saveTime("get deviation")
            let filtered = drinks.filter({ $0.date > reference })
            timer.saveTime("get filtered")
            if minNumber > filtered.count {
                minNumber = filtered.count
            }
            if maxNumber < filtered.count {
                maxNumber = filtered.count
            }
            let sorted = filtered.sorted(by: { $0.date < $1.date })
            timer.saveTime("sorted")
            _ = sorted
        }
        timer.log("tricksForLargeNumbers get \(minNumber) - \(maxNumber) elements")
    }
    
    public func getDataBy(period: TimePeriod) -> [ChartData] {
        sortingQueue.waitUntilAllOperationsAreFinished()
        var drinks = propertysQueue.sync { self.drinks }
        guard !drinks.isEmpty else { return [] }
        
        var data: [ChartData] = []
        
        let component: Calendar.Component
        switch period {
        case .day: component = .day
        case .week: component = .weekOfYear
        case .month: component = .month
        case .year: component = .year
        }
        
        var startDate = Date().round(to: component)
        var endDate = Date()
        
        var currentDrink = drinks.removeLast()
        while drinks.count > 0 {
            var currentValue: Float = 0
            
            while currentDrink.date > startDate {
                currentValue += currentDrink.ethanol
                if drinks.count == 0 {
                    break
                } else {
                    currentDrink = drinks.removeLast()
                }
            }
            data.append(ChartData(startDate: startDate,
                                  endDate: endDate,
                                  value: currentValue,
                                  isHighlighted: startDate.isFirst(in: component)))
            endDate = startDate
            startDate = Calendar.current.date(byAdding: component, value: -1, to: startDate)!
        }
        
        return data
    }
    
    private func queue(at date: Date) -> DrinkQueue? {
        propertysQueue.sync {
            queued.reversed().first(where: { $0.isInclude(date) })
        }
    }
    
    private func detoxSpeed() -> Float {
        guard weight > 1 else {
            return 1000
        }

        switch gender {
        case .male:
            return 0.142 * pow(weight, 0.95)
            
        case .female:
            return 0.12 * pow(weight, 0.95)
            
        case .unknown:
            return 0.13 * pow(weight, 0.95)
        }
    }
    
    // MARK: - Public
    public var currentQueue: DrinkQueuing? {
        queue(at: Date())
    }
    
    public func setup(with drinks: [Drink]) {
        propertysQueue.sync(flags: .barrier) {
            self.drinks = drinks
        }
        sortAndGroup()
    }
    
    public func add(drink: Drink) {
        //TODO optimize
        propertysQueue.sync(flags: .barrier) {
            drinks.append(drink)
        }
        sortAndGroup()
        onDrinksChange()
    }
    
    public func replace(drink: Drink, with newDrink: Drink) {
        //TODO optimize
        let isChanged = propertysQueue.sync(flags: .barrier) { () -> Bool in
            if let index = drinks.firstIndex(of: drink) {
                drinks[index] = newDrink
                return true
            }
            return false
        }
        if isChanged {
            sortAndGroup()
            onDrinksChange()
        }
    }
    
    public func remove(drink: Drink) {
        //TODO optimize
        let isChanged = propertysQueue.sync(flags: .barrier) { () -> Bool in
            if let index = drinks.firstIndex(of: drink) {
                drinks.remove(at: index)
                return true
            }
            return false
        }
        if isChanged {
            sortAndGroup()
            onDrinksChange()
        }
    }
    
    public func updateUserCharacteristics(weight: Float, gender: Gender) {
        guard self.weight != weight || self.gender != gender else {
            return
        }
        self.weight = weight
        self.gender = gender
        sortAndGroup()
    }
    
    public func detoxicationTime(at date: Date) -> Date? {
        queue(at: date)?.detoxDate
    }
    
    public func millilitersInRow(at date: Date) -> Float {
        queue(at: date)?.milliliters.rounded() ?? 0
    }
    
    public func millilitersInBlood(at date: Date) -> Float {
        queue(at: date)?.millilitersInBlood(at: date) ?? 0
    }
    
    public func pointsOfAlcoholInThisWeek() -> Float {
        let startOfWeek = Date().addingTimeInterval(-.week)
        let pointOfAlcohol: Float = 20 //ml
        let filtered = drinks.filter { $0.date > startOfWeek }
        return filtered.reduce(0, { $0 + $1.ethanol }) / pointOfAlcohol
    }
    
    private func onDrinksChange() {
        let drinks = propertysQueue.sync { self.drinks }
        delegate?.drinkManager(drinksChanged: drinks)
    }
}

// MARK: - WarningLevel

public extension DrinkManager {

    enum WarningLevel {
        case none
        case low
        case high
        case extreme
    }

    var warningLevel: WarningLevel {
        let ml = millilitersInBlood(at: Date())

        let mlMax = currentQueue?.expectedMaximum().value ?? 0

        let maximum = max(ml, mlMax)
        if maximum > 150 {
            return .extreme
        } else if maximum > 100 {
            return .high
        } else if maximum > 85 {
            return .low
        } else {
            return .none
        }
    }

}
