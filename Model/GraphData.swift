//
//  GraphData.swift
//  Model
//
//  Created by Матвеев Олег on 31.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public struct DatedValue<T> {
    public let date: Date
    public let value: T
}

public struct GraphData {
    public let points: [DatedValue<Float>]
}

public struct ChartData {
    public let startDate: Date
    public let endDate: Date
    public var value: Float
    public var isHighlighted: Bool
}
