//
//  Gender.swift
//  Model
//
//  Created by kogami on 13.03.2021.
//  Copyright © 2021 Oleg Matveev. All rights reserved.
//

import Foundation

public enum Gender: Codable, Equatable {
    case male
    case female
    case unknown(value: String)
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .male: try? container.encode("male")
        case .female: try? container.encode("female")
        case .unknown(let value): try? container.encode(value)
        }
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let status = try? container.decode(String.self)
        switch status {
        case "male": self = .male
        case "female": self = .female
        default: self = .unknown(value: status ?? "unknown")
        }
    }
}
